package com.paradigma.perseo.streaming.test

import kafka.serializer.StringDecoder
import org.apache.log4j.{ Level, Logger }
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{ DStream, InputDStream }
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{ Milliseconds, StreamingContext }
import org.apache.spark.SparkContext
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector._
import org.apache.spark.sql.SaveMode
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.linalg.Vectors
import com.paradigma.perseo.mail.JavaMailThread
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.feature.StandardScalerModel
import com.paradigma.perseo.streaming.util.SparkConfFactory
import org.apache.spark.mllib.regression.LassoModel
import org.apache.spark.mllib.regression.RidgeRegressionModel
import com.paradigma.perseo.algorithm.rbf.PerseoRBFRegressionModel

object ClassificationTest {

  def main(args: Array[String]) {

    val sparkConf = new SparkConf().setAppName("prueba").setMaster("local[2]").set("spark.ui.port", "7077").set("spark.mesos.coarse", "true")
      .set("spark.cassandra.connection.host", "helper4.stratio.com").set("spark.driver.allowMultipleContexts", "true")

    lazy val sc = new SparkContext(sparkConf)

    var keytime: Int = 3611
    var prediction: Double = 1.0
    var value: Double = 1.0

    while (keytime < 7950) {
      println(keytime)
      CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>

        if (keytime >= 4100 && keytime < 4900) prediction = 3.0
        if (keytime >= 4900 && keytime < 5500) prediction = 5.0
        if (keytime >= 5500 && keytime < 6100) prediction = 2.0
        if (keytime >= 6100 && keytime < 6800) prediction = 7.0
        if (keytime >= 6800 && keytime < 7100) prediction = 4.0
        if (keytime >= 7100 && keytime < 7950) prediction = 6.0

        if (keytime >= 4300 && keytime < 5150) value = 3.0
        if (keytime >= 5150 && keytime < 5500) value = 5.0
        if (keytime >= 5500 && keytime < 6300) value = 2.0
        if (keytime >= 6300 && keytime < 6950) value = 7.0
        if (keytime >= 6950 && keytime < 7290) value = 4.0
        if (keytime >= 7290 && keytime < 7950) value = 6.0
        
        session.execute("CREATE TABLE IF NOT EXISTS perseo.predictionsrealtimeclass(keytime int PRIMARY KEY, valuekey Double, valuepredict Double);")

        session.execute("INSERT INTO perseo.predictionsrealtimeclass (keytime, valuepredict, valuekey) values(" + keytime + "," + prediction + "," + value + ")")
        session.close()

      }
      keytime = keytime + 33
    }

  }

}