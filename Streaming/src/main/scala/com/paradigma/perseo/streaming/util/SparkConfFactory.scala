package com.paradigma.perseo.streaming.util

import org.apache.spark.SparkConf

/**
 * @author jalmodovar
 */
object SparkConfFactory extends Serializable{
  val sparkConf = new SparkConf().setAppName("Streaming").set("spark.ui.port", "7077").set("spark.mesos.coarse", "true")
      .set("spark.cassandra.connection.host", "helper4.stratio.com").set("spark.driver.allowMultipleContexts", "true")
 
}