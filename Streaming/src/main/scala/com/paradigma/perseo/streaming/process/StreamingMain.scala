package com.paradigma.perseo.streaming.process

object StreamingMain {

  def main(args: Array[String]) {
    if (args.length != 8) {
      System.err.println(
        s"""
           |Usage: StreamingMode <number-of-samples> <sampling-period> <type> <algorithm> <unitTime> <mailTo> <valueAlert> <valuesToPredict>
           |  <number-of-samples> is the number of samples in each windows
           |  <sampling-period> period between samples in miliseconds
           |  <type> is the form to predict. classification | regression
           |  <algorithm> is regression -> (linearRegression-sgd, linearRegression-ols, ridge, lasso or rbf). Classification -> (decisionTree, logisticMulticlass, naiveBayes or randomForest) 
           |  <unitTime> is the unit between events
           |  <mailTo> is the email to send the alerts
           |  <valueAlert> is the value to check the alerts
           |  <valuesToPredict> is the number of values to predict
           |
        """.stripMargin)
      System.exit(1)
    } else {
      if (args.apply(0).toDouble <= 0) {
        System.err.println("<number-of-samples> is not valid: " + args.apply(0).toString() + ". Must be and Integer > 0")
        System.exit(1)
      } else if (args.apply(4).toInt <= 0) {
        System.err.println("<unitTime> is not valid: " + args.apply(4).toString() + ". Must be and Integer > 0")
        System.exit(1)
      } else if (!args.apply(2).toString().toLowerCase().equals("regression") && !args.apply(2).toString().toLowerCase().equals("classification")) {
        System.err.println("<type> is not valid: " + args.apply(2).toString().toLowerCase() + ". Must choose classification|regression")
        System.exit(1)
      } else if (args.apply(2).toString().toLowerCase().equals("regression") && (!args.apply(3).toString().toLowerCase().equals("linearregression-ols") && !args.apply(3).toString().toLowerCase().equals("linearregression-sgd") && !args.apply(3).toString().toLowerCase().equals("rbf") && !args.apply(3).toString().toLowerCase().equals("lasso") && !args.apply(3).toString().toLowerCase().equals("ridge"))) {
        System.err.println("<algorithm> is not valid: " + args.apply(3).toString().toLowerCase() + ". When you choose regression you must choose linearRegression, ridge, lasso or rbf")
        System.exit(1)
      } else if (args.apply(2).toString().toLowerCase().equals("classification") && (!args.apply(3).toString().toLowerCase().equals("decisiontree") && !args.apply(3).toString().toLowerCase().equals("logisticmulticlass") && !args.apply(3).toString().toLowerCase().equals("randomforest") && !args.apply(3).toString().toLowerCase().equals("naivebayes"))) {
        System.err.println("<algorithm> is not valid: " + args.apply(3).toString().toLowerCase() + ". When you choose classification you must choose decisionTree, logisticMulticlass, naiveBayes or randomForest")
        System.exit(1)
      } else if (!args.apply(5).toString().toLowerCase().contains("@")) {
        System.err.println("<mailTo> is not valid: " + args.apply(5).toString().toLowerCase())
        System.exit(1)
      } else if (args.apply(7).toInt <= 0) {
        System.err.println("<valuesToPredict> is not valid: " + args.apply(7).toString() + ". Must be and Integer > 0")
        System.exit(1)
      } else {

        if (args.apply(2).toString().toLowerCase().equals("regression")) {
          StreamingRegressionProcess.process(args.apply(0).toDouble, args.apply(1).toDouble, args.apply(3).toString().toLowerCase(), args.apply(4).toInt, args.apply(5).toString().toLowerCase(), args.apply(6).toDouble, args.apply(7).toInt)
        } else if (args.apply(2).toString().toLowerCase().equals("classification")) {
          StreamingClassificationProcess.process(args.apply(0).toDouble, args.apply(1).toDouble, args.apply(3).toString().toLowerCase(), args.apply(4).toInt, args.apply(5).toString().toLowerCase(), args.apply(6).toDouble, args.apply(7).toInt)
        }
      }
    }
  }

}