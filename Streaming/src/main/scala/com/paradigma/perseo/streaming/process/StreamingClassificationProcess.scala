package com.paradigma.perseo.streaming.process

import kafka.serializer.StringDecoder
import org.apache.log4j.{ Level, Logger }
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{ DStream, InputDStream }
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{ Milliseconds, StreamingContext }
import org.apache.spark.SparkContext
import com.paradigma.perseo.streaming.util.SparkConfFactory
import com.paradigma.perseo.algorithm.util.Complex
import com.paradigma.perseo.algorithm.util.FFT
import org.apache.spark.mllib.linalg.Vectors
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector._
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.classification.PerseoNaiveBayes
import org.apache.spark.mllib.classification.NaiveBayesModel
import com.paradigma.perseo.algorithm.classification.PerseoRandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import com.paradigma.perseo.algorithm.classification.PerseoGradientBoostedTrees
import org.apache.spark.mllib.tree.model.GradientBoostedTreesModel
import com.paradigma.perseo.algorithm.classification.PerseoLogisticMulticlassification
import org.apache.spark.mllib.classification.LogisticRegressionModel

object StreamingClassificationProcess {
  def process(numberOfSamples: Double, samplingPeriod: Double, algorithm: String, unitTime: Int, mailTo: String, valueAlert: Double, valuesToPredict: Int) {
    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    val topics = "topicClassificationPerseo"
    val brokers = "helper3.stratio.com:9092, helper2.stratio.com:9092"

    val numClasses = 7

    val frequencyInSec = 5
    val intervalInSec = 10
    val samples = frequencyInSec * intervalInSec

    val longFft = FFT.convertPow2(samples)

    val vectorFrequency = (-frequencyInSec / 2.0 to frequencyInSec / 2.0).by(frequencyInSec.toDouble / (longFft - 1)).toArray.takeRight(longFft / 2)

    val path = "/tmp/modelclass/"

    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("CREATE KEYSPACE IF NOT EXISTS perseo WITH replication = {'class': 'SimpleStrategy','replication_factor': '1'};")
      session.execute("USE perseo;")
      session.execute("DROP TABLE IF EXISTS predictionsrealtimeclass;")
      session.execute("CREATE TABLE IF NOT EXISTS predictionsrealtimeclass(keytime int PRIMARY KEY, valuekey Double, valuepredict Double);")

      session.close();
    }

    // Create context with N millisecond batch interval
    val interval = (samplingPeriod.toDouble).toInt
    val ssc = new StreamingContext(SparkConfFactory.sparkConf, Milliseconds(interval))
    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)
    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaParams, topicsSet).map(_._2)

    messages.count.map(cnt => "The number of received messages is " + cnt.toString).print
    //messages.foreachRDD(_.foreach { println(_) })

    val messagesInWindows = messages.map({ message =>
      message.split(",")
      //List[String](splitData.apply(0).trim())
    })

    /*val window = (interval * numberOfSamples * 1.020).toInt
    val events: DStream[String] = messages.window(Milliseconds(window), Milliseconds(interval))

    val data: DStream[Array[String]] = events.map({ message =>
      message.split(",")
      //Array[String](splitData.apply(0).trim() -> splitData.apply(1).trim() -> splitData.apply(2).trim() -> splitData.apply(3).trim() -> splitData.apply(4).trim())
    })*/

    messagesInWindows.foreachRDD { rdd =>
      val transformArray = rdd.collect().map(row => (row.apply(0).trim().toInt, row.apply(1).trim().toDouble, row.apply(2).trim().toDouble, row.apply(3).trim().toDouble, row.apply(4).trim().toDouble)).reverse.sliding(numberOfSamples.toInt + 1).map({
        case list => ((list.head._1), list.drop(1).toList.map({ case (timestamp, x, y, z, target) => x + "," + y + "," + z }), list.drop(1).toList.map({ case (timestamp, x, y, z, target) => target }))
      }).filter { case (key, listValue, listTargets) => listValue.size == numberOfSamples }.map { case (key, listValue, listTargets) => ((key, getMode(listTargets)), listValue.mkString("@")) }.toArray.sortBy(_._1) //.foreach(println)
      if (transformArray.length >= 1) {
        val dataPredict = transformArray.apply(transformArray.length - 1)
        //for (dataPredict <- transformArray) {

        val dataX = (dataPredict._1._1, dataPredict._1._2, dataPredict._2.split("@").map { x => new Complex(x.split(",").apply(0).toDouble, 0) }.toList)
        val dataY = (dataPredict._1._1, dataPredict._1._2, dataPredict._2.split("@").map { x => new Complex(x.split(",").apply(1).toDouble, 0) }.toList)
        val dataZ = (dataPredict._1._1, dataPredict._1._2, dataPredict._2.split("@").map { x => new Complex(x.split(",").apply(2).toDouble, 0) }.toList)

        val timeKey = dataPredict._1._1
        val valueKey = dataPredict._1._2
        val timedLabel = (dataPredict._1._1, dataPredict._1._2 - 1)

        val dataFttModuleX = (dataX._1, dataX._2, FFT.executeFFT(dataX._3.toList).map { x => x.module() }.takeRight(longFft / 2)) //rddDataX.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
        val dataFttModuleY = (dataY._1, dataY._2, FFT.executeFFT(dataY._3.toList).map { x => x.module() }.takeRight(longFft / 2)) //rddDataY.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
        val dataFttModuleZ = (dataZ._1, dataZ._2, FFT.executeFFT(dataZ._3.toList).map { x => x.module() }.takeRight(longFft / 2)) //rddDataZ.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }

        val meanAndStdX = (dataX._1, (calculateMean(dataX._3), calculateStd(dataX._3))) //rddDataX.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }
        val meanAndStdY = (dataY._1, (calculateMean(dataY._3), calculateStd(dataY._3)))
        val meanAndStdZ = (dataY._1, (calculateMean(dataZ._3), calculateStd(dataZ._3)))

        val maxAndPosFreqX = (dataFttModuleX._1, (dataFttModuleX._3.max, calculatePointMax(dataFttModuleX._3.max, dataFttModuleX._3, vectorFrequency)))
        /*dataFttModuleX.map {
          case (timestamp, target, listModuleComplex) =>
            val max = listModuleComplex.max
            (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
          }*/

        val maxAndPosFreqY = (dataFttModuleY._1, (dataFttModuleY._3.max, calculatePointMax(dataFttModuleY._3.max, dataFttModuleY._3, vectorFrequency)))
        val maxAndPosFreqZ = (dataFttModuleZ._1, (dataFttModuleZ._3.max, calculatePointMax(dataFttModuleZ._3.max, dataFttModuleZ._3, vectorFrequency)))

        val bandwidth50And95PosFreqX = (dataFttModuleX._1, (caculateFreqPositionBandwidth(50, dataFttModuleX._3, vectorFrequency), caculateFreqPositionBandwidth(95, dataFttModuleX._3, vectorFrequency)))
        /*dataFttModuleX.map {
          case (timestamp, target, listModuleComplex) =>
            (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
        }*/

        val bandwidth50And95PosFreqY = (dataFttModuleY._1, (caculateFreqPositionBandwidth(50, dataFttModuleY._3, vectorFrequency), caculateFreqPositionBandwidth(95, dataFttModuleY._3, vectorFrequency)))
        val bandwidth50And95PosFreqZ = (dataFttModuleZ._1, (caculateFreqPositionBandwidth(50, dataFttModuleZ._3, vectorFrequency), caculateFreqPositionBandwidth(95, dataFttModuleZ._3, vectorFrequency)))

        // features mean, std, max, posFreq max, posFreq bw50, posFreq bw95
        val featuresX = (meanAndStdX, maxAndPosFreqX, bandwidth50And95PosFreqX) //meanAndStdX.zip(maxAndPosFreqX).zip(bandwidth50And95PosFreqX)
        val featuresY = (meanAndStdX, maxAndPosFreqX, bandwidth50And95PosFreqX)
        val featuresZ = (meanAndStdX, maxAndPosFreqX, bandwidth50And95PosFreqX)

        val testData = Vectors.dense(Array(featuresX._1._2._1, featuresX._1._2._2, featuresX._2._2._1, featuresX._2._2._2, featuresX._3._2._1, featuresX._3._2._2,
          featuresY._1._2._1, featuresY._1._2._2, featuresY._2._2._1, featuresY._2._2._2, featuresY._3._2._1, featuresY._3._2._2,
          featuresZ._1._2._1, featuresZ._1._2._2, featuresZ._2._2._1, featuresZ._2._2._2, featuresZ._3._2._1, featuresZ._3._2._2))
        /* val testData = arrayTimedLabel.zip(featuresX).zip(featuresY).zip(featuresZ).map {
          case ((((timestamp, label), (((tmx1, (meanX, stdX)), (tmx2, (maxX, pfreqX))), (tmx3, (bw50X, bx95X)))), (((tmy1, (meanY, stdY)), (tmy2, (maxY, pfreqY))), (tmy3, (bw50Y, bw95Y)))), (((tmz1, (meanZ, stdZ)), (tmz2, (maxZ, pfreqZ))), (tmz3, (bw50Z, bw95Z)))) =>
            val arrayValues = Array(meanX, stdX, maxX, pfreqX, bw50X, bx95X, meanY, stdY, maxY, pfreqY, bw50Y, bw95Y, meanZ, stdZ, maxZ, pfreqZ, bw50Z, bw95Z)
            Vectors.dense(arrayValues)
          //new TimedLabeledPoint(timestamp, label, features)
        }*/

        val predict: Double = if (algorithm.equals("decisiontree")) {
          val modelTraining = DecisionTreeModel.load(rdd.context, path + "decisiontree")
          modelTraining.predict(testData) + 1
        } else if (algorithm.equals("logisticmulticlass")) {
          val modelTraining = LogisticRegressionModel.load(rdd.context, path + "logisticmulticlass")
          modelTraining.predict(testData) + 1
        } else if (algorithm.equals("naivebayes")) {
          val modelTraining = NaiveBayesModel.load(rdd.context, path + "naivebayes")
          modelTraining.predict(testData) + 1

        } else if (algorithm.equals("randomforest")) {
          val modelTraining = RandomForestModel.load(rdd.context, path + "randomforest")
          modelTraining.predict(testData) + 1
        } else {
          0.0
        }

        println("predict: " + predict + ", in time: " + timedLabel._1)

        CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>

          //session.execute("INSERT INTO perseo.datatraining (keytime, valuepredict, valuesbefore) values(" + dataPredict._1._1.toInt + "," +prediction + ",'" + dataPredict._2 + "')")
          //session.execute("UPDATE perseo.datatraining SET valuekey="+dataPredict._1._2+"WHERE keytime="+(dataPredict._1._1-unitTime).toString()+";")
          session.execute("INSERT INTO perseo.predictionsrealtimeclass (keytime, valuepredict, valuekey) values(" + timeKey + "," + predict + "," + valueKey + ")")

          session.close()
        }

        /*
      --- 1: Working at Computer
      --- 2: Standing Up, Walking and Going up\down stairs
      --- 3: Standing
      --- 4: Walking
      --- 5: Going Up\Down Stairs
      --- 6: Walking and Talking with Someone
      --- 7: Talking while Standing
      */

      }
    }

    // Start the computation
    ssc.start()
    ssc.awaitTermination()

  }

  private def getMode(input: List[Double]): Double = {
    input.groupBy(x => x).mapValues(_.length).toArray.sortBy(_._2).reverse(0)._1
  }

  private def calculateMean(listComplex: List[Complex]): Double = {
    listComplex.map(_.re).sum / listComplex.length
  }

  private def calculateStd(listComplex: List[Complex]): Double = {
    val mean = calculateMean(listComplex)
    Math.sqrt(listComplex.map(_.re).map { x => (x - mean) * (x - mean) }.sum) / listComplex.length
  }

  private def calculatePointMax(max: Double, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    vectorFrequency.apply(listModuleComplex.indexOf(max))
  }

  private def caculateFreqPositionBandwidth(percentage: Int, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    val totalPercentageBandwidth = (listModuleComplex.sum * percentage) / 100.0
    var sumBandwidth = 0.0
    var position = -1
    for (i <- 0 to listModuleComplex.size - 1) {
      sumBandwidth += listModuleComplex.apply(i)

      if (position == -1) {
        if (sumBandwidth >= totalPercentageBandwidth) {
          position = i
        }
      }
    }
    vectorFrequency.apply(position)
  }
}