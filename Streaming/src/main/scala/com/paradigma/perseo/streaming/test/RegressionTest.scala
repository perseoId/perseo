package com.paradigma.perseo.streaming.test

import kafka.serializer.StringDecoder
import org.apache.log4j.{ Level, Logger }
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{ DStream, InputDStream }
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{ Milliseconds, StreamingContext }
import org.apache.spark.SparkContext
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector._
import org.apache.spark.sql.SaveMode
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.mllib.linalg.Vectors
import com.paradigma.perseo.mail.JavaMailThread
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.feature.StandardScalerModel
import com.paradigma.perseo.streaming.util.SparkConfFactory
import org.apache.spark.mllib.regression.LassoModel
import org.apache.spark.mllib.regression.RidgeRegressionModel
import com.paradigma.perseo.algorithm.rbf.PerseoRBFRegressionModel

object RegressionTest {

  def main(args: Array[String]) {

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    val topics = "topicRegressionPerseo"
    val brokers = "helper3.stratio.com:9092, helper2.stratio.com:9092"
    val algorithm = "lasso"
    val numberOfSamples = 10
    val valuesToPredict = 3
    val unitTime = 1

    val seqOpModelLinear = (x: (Double, Double, Array[Double], Double, Array[Vector]), y: (Double, Double, Array[Double], Double, Array[Vector])) => {
      if (y._1 > x._1) y
      else x
    }

    val seqOPScaler = (x: (Double, Array[Double], Array[Double], Boolean, Boolean), y: (Double, Array[Double], Array[Double], Boolean, Boolean)) => {
      if (y._1 > x._1) y
      else x
    }
    //(timeTraining, intercept, weights, sigma, centres)
    val modelTrainingData: (Double, Double, Array[Double], Double, Array[Vector]) = CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>

      val result: (Double, Double, Array[Double], Double, Array[Vector]) = if (algorithm.equals("linearregression-sgd")) {
        sc.cassandraTable("perseo", "modelregression").where("typemodel='SGD'")
          .map { row => (row.getInt("timetraining").toDouble, row.getDouble("intercept"), row.getString("weights").split("@").map { value => value.toDouble }, 0.0, null) }
          .aggregate((-1.0 / 0.0, 0.0, Array[Double](), 0.0, Array[Vector]()))(seqOpModelLinear, seqOpModelLinear)
      } else if (algorithm.equals("linearregression-ols")) {
        sc.cassandraTable("perseo", "modelregression").where("typemodel='OLS'")
          .map { row => (row.getInt("timetraining").toDouble, row.getDouble("intercept"), row.getString("weights").split("@").map { value => value.toDouble }, 0.0, null) }
          .aggregate((-1.0 / 0.0, 0.0, Array[Double](), 0.0, Array[Vector]()))(seqOpModelLinear, seqOpModelLinear)
      } else if (algorithm.equals("ridge")) {
        sc.cassandraTable("perseo", "modelregression").where("typemodel='RIDGE'")
          .map { row => (row.getInt("timetraining").toDouble, row.getDouble("intercept"), row.getString("weights").split("@").map { value => value.toDouble }, 0.0, null) }
          .aggregate((-1.0 / 0.0, 0.0, Array[Double](), 0.0, Array[Vector]()))(seqOpModelLinear, seqOpModelLinear)
      } else if (algorithm.equals("lasso")) {
        sc.cassandraTable("perseo", "modelregression").where("typemodel='LASSO'")
          .map { row => (row.getInt("timetraining").toDouble, row.getDouble("intercept"), row.getString("weights").split("@").map { value => value.toDouble }, 0.0, null) }
          .aggregate((-1.0 / 0.0, 0.0, Array[Double](), 0.0, Array[Vector]()))(seqOpModelLinear, seqOpModelLinear)
      } else if (algorithm.equals("rbf")) {
        sc.cassandraTable("perseo", "modelregression").where("typemodel='RBF'")
          .map { row => (row.getInt("timetraining").toDouble, row.getDouble("intercept"), row.getString("weights").split("@").map { value => value.toDouble }, row.getDouble("sigma"), row.getString("centres").split("@").map { x => Vectors.dense(x.replace("[", "").replace("]", "").split(",").map { x => x.toDouble }) }) }
          .aggregate((-1.0 / 0.0, 0.0, Array[Double](), 0.0, Array[Vector]()))(seqOpModelLinear, seqOpModelLinear)
      } else {
        null
      }
      result
    }

    val auxScaler = sc.cassandraTable("perseo", "scalermodel").where("algorithm='" + algorithm + "'")
      .map { row => (row.getInt("timemodel").toDouble, row.getString("std").split("@").map { value => value.toDouble }, row.getString("mean").split("@").map { value => value.toDouble }, row.getBoolean("withstd"), row.getBoolean("withmean")) }
      .aggregate((-1.0 / 0.0, Array[Double](), Array[Double](), false, false))(seqOPScaler, seqOPScaler)
    val scalerModel: StandardScalerModel = new StandardScalerModel(Vectors.dense(auxScaler._2), Vectors.dense(auxScaler._3), auxScaler._4, auxScaler._5)

    //Load de models

    // (val weights: Vector, val intercept: Double)  
    val modelLinearRegression: LinearRegressionModel = if (algorithm.contains("linearregression")) {
      new LinearRegressionModel(Vectors.dense(modelTrainingData._3), modelTrainingData._2)

    } else { null }

    // (val weights: Vector, val intercept: Double)  
    val modelLasso: LassoModel = if (algorithm.equals("lasso")) {
      new LassoModel(Vectors.dense(modelTrainingData._3), modelTrainingData._2)

    } else { null }

    // (val weights: Vector, val intercept: Double)  
    val modelRidge: RidgeRegressionModel = if (algorithm.equals("ridge")) {
      new RidgeRegressionModel(Vectors.dense(modelTrainingData._3), modelTrainingData._2)

    } else { null }

    // (val centres: Array[Vector], val sigma: Double, val weights: Vector, val intercept: Double)
    val modelRBF: PerseoRBFRegressionModel = if (algorithm.equals("rbf")) {
      new PerseoRBFRegressionModel(modelTrainingData._5, modelTrainingData._4, Vectors.dense(modelTrainingData._3), modelTrainingData._2)
    } else { null }

    //end load models

    // Create context with N millisecond batch interval
    val interval = (10000.0).toInt
    val ssc = new StreamingContext(SparkConfFactory.sparkConf, Milliseconds(interval))
    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)
    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaParams, topicsSet).map(_._2)

    messages.count.map(cnt => "The number of received messages is " + cnt.toString).print
    messages.foreachRDD(_.foreach { println(_) })

    val messagesInWindows = messages.flatMap({ message =>
      val splitData = message.split(",")
      List[String](splitData.apply(0).trim())
    })

    val window = (interval * numberOfSamples * 1.2).toInt
    val events: DStream[String] = messages.window(Milliseconds(window), Milliseconds(interval))

    val data: DStream[Map[String, String]] = events.map({ message =>
      val splitData = message.split(",")
      Map[String, String](splitData.apply(0).trim() -> splitData.apply(1).trim())
    })

    data.foreachRDD { rdd =>
      val transformArray = rdd.collect.map(row => (row.head, row.last)).reverse.sliding(numberOfSamples.toInt).map({
        case list => (list.head._1, list.toList.map({ case (timestamp, value) => value._2 }))
      }).filter { case (key, listValue) => listValue.size == numberOfSamples }.map { case (key, listValue) => (((key._1.toInt + unitTime), key._2), listValue.mkString("@")) }.toArray.sortBy(_._1) //.foreach(println)

      var keytime = 0
      var valueKey = 0.0
      var prediction = 0.0
      var featuresAux = Array.emptyDoubleArray

      for (dataPredict <- transformArray) {

        println("data to Predict: " + dataPredict)
        for (i <- 1 to valuesToPredict) {
          val features = dataPredict._2.split("@").map { x => x.toDouble }


          if (i == 1) {
            keytime = dataPredict._1._1.toInt
            valueKey = dataPredict._1._2.toDouble

            prediction = if (algorithm.contains("linearregression")) { modelLinearRegression.predict(scalerModel.transform(Vectors.dense(features))) }
            else if (algorithm.equals("rbf")) { modelRBF.predict(scalerModel.transform(Vectors.dense(features))) }
            else if (algorithm.equals("lasso")) { modelLasso.predict(scalerModel.transform(Vectors.dense(features))) }
            else if (algorithm.equals("ridge")) { modelRidge.predict(scalerModel.transform(Vectors.dense(features))) }
            else { 0.0 }
            println("prediction for time: " + keytime + " -> " + prediction)
            featuresAux = (Array(prediction) ++: features).dropRight(1)
          } else {
            keytime = keytime + unitTime
            valueKey = prediction
            println("features: " + featuresAux)
            println("ScalerModel: " + scalerModel)
            prediction = if (algorithm.contains("linearregression")) { modelLinearRegression.predict(scalerModel.transform(Vectors.dense(featuresAux))) }
            else if (algorithm.equals("rbf")) { modelRBF.predict(scalerModel.transform(Vectors.dense(featuresAux))) }
            else if (algorithm.equals("lasso")) { modelLasso.predict(scalerModel.transform(Vectors.dense(featuresAux))) }
            else if (algorithm.equals("ridge")) { modelRidge.predict(scalerModel.transform(Vectors.dense(featuresAux))) }
            else { 0.0 }
            println("prediction for time: " + keytime + " -> " + prediction)
            featuresAux = (Array(prediction) ++: features).dropRight(1)
          }

        }
      }

      //transformArray.foreach(println)

    }

    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }

}