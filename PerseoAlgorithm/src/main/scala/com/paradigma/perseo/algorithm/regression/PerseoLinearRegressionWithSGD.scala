package com.paradigma.perseo.algorithm.regression

import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.regression.LinearRegressionModel
import com.paradigma.perseo.algorithm.util.AlgorithmUtil
import org.apache.spark.mllib.linalg.Vector

object PerseoLinearRegressionWithSGD extends LinearRegressionWithSGD {
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double): LinearRegressionModel={
    LinearRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int): LinearRegressionModel={
    LinearRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, miniBatchFraction:Double): LinearRegressionModel={
    LinearRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, miniBatchFraction)
  }
  
    def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, miniBatchFraction:Double, initialWeights:Vector): LinearRegressionModel={
    LinearRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, miniBatchFraction,initialWeights)
  }
}