package com.paradigma.perseo.algorithm.util

import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, IndexedRowMatrix, MatrixEntry, RowMatrix}
import org.apache.spark.mllib.linalg.{DenseMatrix, DenseVector, Matrix, Vector}

object MatrixUtils {

  def orderedMatrixProduct(x: IndexedRowMatrix, y: IndexedRowMatrix): IndexedRowMatrix = {

    val numColsX = x.numCols()
    val numRowsY = y.numRows()

    if (numColsX != numRowsY)
      sys.error(s"Matrix dimensions are not correct (IndexedRowMatrix).")

    val transY = y.toCoordinateMatrix().transpose().toIndexedRowMatrix()

    val cart = x.rows.cartesian(transY.rows)

    val entries = cart
      .map{case(row_i, col_j) => new MatrixEntry(row_i.index, col_j.index, innerProd(row_i.vector, col_j.vector))}

    new CoordinateMatrix(entries).toIndexedRowMatrix()

  }

  def orderedMatrixProduct(x: CoordinateMatrix, y: CoordinateMatrix): CoordinateMatrix = {

    val numColsX = x.numCols()
    val numRowsY = y.numRows()

    if (numColsX != numRowsY)
      sys.error(s"Matrix dimensions are not correct.")

    val entriesX = x.entries.map(x => (x.j, x))
    val entriesY = y.entries.map(y => (y.i, y))

    val products = entriesX.join(entriesY)
      .map{case(key, (entryX, entryY)) => ((entryX.i, entryY.j), entryX.value*entryY.value)}

    val initialValue = 0.0
    val addToSet = (sum: Double, newValue: Double) => sum + newValue
    val mergePartitionSets = (sum1: Double, sum2: Double) => sum1 + sum2

    val entriesResult = products.aggregateByKey(initialValue)(addToSet, mergePartitionSets)
      .map{case((row, column), value) => new MatrixEntry(row, column, value)}

    new CoordinateMatrix(entriesResult)

  }

  private def innerProd(x: Vector, y: Vector): Double = (x.toArray zip y.toArray).map{case(a,b) => a*b}.sum

  def getPseudoInverseMatrix(input: RowMatrix): DenseMatrix = {
    val nCoef = input.numCols.toInt
    val svd = input.computeSVD(nCoef, computeU = true)
    // svd.s.size: number of non-zero singular values
    if (svd.s.size < nCoef) {
      // sys.error(s"RowMatrix.computeInverse called on singular matrix.")
      println("svd.s.size < nCoef (RowMatrix)")
      println("size(svd.s): " + svd.s.size)
      println("cols(svd.U): " + svd.U.numCols())
      println("rows(svd.U): " + svd.U.numRows())
      println("cols(svd.V): " + svd.V.numCols)
      println("rows(svd.V): " + svd.V.numRows)
    }

    // Create the inv diagonal matrix from S; local matrix to use the multiply method
    val invS = DenseMatrix.diag(new DenseVector(svd.s.toArray.map(x => math.pow(x,-1))))

    // U cannot be a RowMatrix; local matrix to use the multiply method
    val U = new DenseMatrix(svd.U.numRows().toInt,svd.U.numCols().toInt,svd.U.rows.collect.flatMap(x => x.toArray))

    // If you could make V distributed, then this may be better. However its alreadly local...so maybe this is fine.
    val V = svd.V
    // inv(X) = V*inv(S)*transpose(U)  --- the U is already transposed.
    (V.multiply(invS)).multiply(U)
  }

  def getPseudoInverseMatrix(input: IndexedRowMatrix): IndexedRowMatrix = {
    val sc = input.rows.sparkContext
    val nCoef = input.numCols.toInt
    val svd = input.computeSVD(nCoef, computeU = true)
    // svd.s.size: number of non-zero singular values

    if (svd.s.size < nCoef) {
      // sys.error(s"RowMatrix.computeInverse called on singular matrix.")
      println("svd.s.size < nCoef (IndexedRowMatrix)")
      println("size(svd.s): " + svd.s.size)
      println("cols(svd.U): " + svd.U.numCols())
      println("rows(svd.U): " + svd.U.numRows())
      println("cols(svd.V): " + svd.V.numCols)
      println("rows(svd.V): " + svd.V.numRows)
    }

    // Create the inv diagonal matrix from S

    val dimS = svd.s.size

    val entriesInvS = for{i <- (0 to (dimS-1)).toArray; j <- (0 to (dimS-1)).toArray} yield {
      if(i == j) new MatrixEntry(i,j,math.pow(svd.s.toArray(i),-1))
      else new MatrixEntry(i,j,0)
    }

    val invS = new CoordinateMatrix(sc.parallelize(entriesInvS))

    val U = svd.U

    val uTrans = U.toCoordinateMatrix().transpose()

    // If you could make V distributed, then this may be better. However its alreadly local...so maybe this is fine.
    val V = convertLocalMatrixToCoordinateMatrix(sc, svd.V)
    // inv(X) = V*inv(S)*transpose(U)  --- the U is already transposed.

    val temp = orderedMatrixProduct(V, invS)

    val result = orderedMatrixProduct(temp, uTrans)

    result.toIndexedRowMatrix()
  }

  def getPseudoInverseMatrix(input: CoordinateMatrix): CoordinateMatrix = getPseudoInverseMatrix(input
    .toIndexedRowMatrix()).toCoordinateMatrix()

  def convertRowMatrixToCoordinateMatrix(rowmatrix: RowMatrix): CoordinateMatrix = {
    val rows = rowmatrix.rows

    val rddEntries = rows.zipWithIndex().flatMap{case(vector, row) => {

      val arrayOfEntries = vector.toArray.zipWithIndex.map{case(value, column) => new MatrixEntry(row, column, value)}

      arrayOfEntries
    }
    }

    new CoordinateMatrix(rddEntries)
  }

  def convertLocalMatrixToCoordinateMatrix(sc: SparkContext, matrix: Matrix): CoordinateMatrix = {
    val row = (0 to (matrix.numRows - 1)).toArray
    val col = (0 to (matrix.numCols - 1)).toArray

    val entries = for(i <- row; j <- col) yield new MatrixEntry(i,j,matrix.apply(i,j))

    new CoordinateMatrix(sc.parallelize(entries))

  }



}
