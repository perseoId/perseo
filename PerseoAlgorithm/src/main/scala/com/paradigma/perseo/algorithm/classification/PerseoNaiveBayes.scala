package com.paradigma.perseo.algorithm.classification

import org.apache.spark.mllib.classification.NaiveBayes
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.classification.NaiveBayesModel
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.regression.LabeledPoint

object PerseoNaiveBayes extends NaiveBayes {

  def train(input: RDD[TimedLabeledPoint], lambda: Double, modelType: String): NaiveBayesModel = {
    NaiveBayes.train(input.map { tlp => LabeledPoint(tlp.label, tlp.features) }, lambda, modelType)
  }
  def train(input: RDD[TimedLabeledPoint], lambda: Double): NaiveBayesModel = {
    NaiveBayes.train(input.map { tlp => LabeledPoint(tlp.label, tlp.features) }, lambda)
  }
  def train(input: RDD[TimedLabeledPoint]): NaiveBayesModel = {
    NaiveBayes.train(input.map { tlp => LabeledPoint(tlp.label, tlp.features) })
  }

}