package com.paradigma.perseo.algorithm.classification

import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.tree.configuration.Strategy
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.model.RandomForestModel

object PerseoRandomForest {
  def trainClassifier(input: RDD[TimedLabeledPoint], numClasses: Int, categoricalFeaturesInfo: Map[Int, Int], numTrees: Int, featureSubsetStrategy: String, impurity: String, maxDepth: Int, maxBins: Int, seed: Int): RandomForestModel ={
    RandomForest.trainClassifier(input.map { tlp => LabeledPoint(tlp.label, tlp.features)}, numClasses, categoricalFeaturesInfo, numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins, seed)
  }
  
  def trainClassifier(input: RDD[TimedLabeledPoint], strategy: Strategy, numTrees: Int, featureSubsetStrategy: String, seed: Int): RandomForestModel = {
    RandomForest.trainClassifier(input.map { tlp => LabeledPoint(tlp.label, tlp.features)}, strategy, numTrees, featureSubsetStrategy, seed)
  }
    
}