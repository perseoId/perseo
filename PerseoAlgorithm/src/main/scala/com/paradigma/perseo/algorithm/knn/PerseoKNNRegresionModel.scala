package com.paradigma.perseo.algorithm.knn

import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.{Vectors, Vector}
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint

trait PerseoKNNRegresionModel extends Serializable {
  
  def predict(input: Vector, training: RDD[TimedLabeledPoint]): Double 
  
  def predict(input: RDD[TimedLabeledPoint], training: RDD[TimedLabeledPoint]): RDD[(TimedLabeledPoint, Double)]
  
}