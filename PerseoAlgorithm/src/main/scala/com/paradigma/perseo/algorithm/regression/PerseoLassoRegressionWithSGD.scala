package com.paradigma.perseo.algorithm.regression

import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.util.AlgorithmUtil
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LassoWithSGD
import org.apache.spark.mllib.regression.LassoModel


object PerseoLassoRegressionWithSGD extends LassoWithSGD {
  def train(input: RDD[TimedLabeledPoint], numIterations: Int): LassoModel={
    LassoWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double): LassoModel={
    LassoWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double, miniBatchFraction:Double): LassoModel={
    LassoWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam, miniBatchFraction)
  }
  
    def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double, miniBatchFraction:Double, initialWeights:Vector): LassoModel={
    LassoWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam, miniBatchFraction, initialWeights)
  }
}