package com.paradigma.perseo.algorithm.util

import scala.math._

object FFT {

  def executeFFT(listComplex: List[Complex]): List[Complex] = {
    computeFFT(convertPow2(listComplex))
  }
  
  def convertPow2(sizeList: Int): Int = {
     if (isPow2(sizeList)) { return sizeList }
     else{return convertPow2(sizeList+1)}
  }

  private def computeFFT(listComplex: List[Complex]): List[Complex] = {

    val pi = 3.141592
    listComplex.length match {

      case 0 => Nil

      case 1 => listComplex

      case n => {

        val even = computeFFT(listComplex.zipWithIndex.filter { case (v, i) => (i % 2) == 0 }.map(_._1))

        val odd = computeFFT(listComplex.zipWithIndex.filter { case (v, i) => (i % 2) != 0 }.map(_._1))

        def fftAccum(accum: List[(Int, Complex)], freq: Int = 0): List[(Int, Complex)] = {

          val value = new Complex(cos(-2 * pi * freq / n.toDouble), sin(-2 * pi * freq / n.toDouble))

          freq < (n.toDouble / 2.0) match {
            case true =>
              fftAccum(List((freq, even(freq) + value * odd(freq))) ++ List((freq + n / 2, even(freq) - value * odd(freq))) ++ accum, freq + 1)

            case false => accum
          }
        }

        fftAccum(List[(Int, Complex)]()).sortWith((x, y) => x._1 < y._1).map(_._2)

      }
    }
  }

  private def myexp(complex: Complex): Complex = {
    val r = (cosh(complex.re) + sinh(complex.re))
    new Complex(cos(complex.im), sin(complex.im)) //* r
  }

  private def isPow2(value: Int): Boolean = {
    if (value == 1) { return true; }
    else if (value % 2 != 0 || value < 1) { return false; }
    else { return isPow2(value / 2); }

  }

  private def convertPow2(listComplex: List[Complex]): List[Complex] = {
    if (isPow2(listComplex.size)) { return listComplex }
    else {
      var mutableSeq = scala.collection.mutable.ArraySeq(listComplex: _*)
      mutableSeq = mutableSeq.+:(new Complex(0, 0))
      return convertPow2(mutableSeq.toList)
    }
  }
}