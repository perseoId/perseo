package com.paradigma.perseo.algorithm.rbf

import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.VectorProjector
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithOLS


object PerseoRBFRegression {

  def train(data: RDD[TimedLabeledPoint], numHiddenNeurons: Int, sigma: Double): PerseoRBFRegressionModel = {
    // Unsupervised definition of centres in the projected space
    val maxNumIters = 500

    val centres = KMeans.train(data.map(tlp=>tlp.features), numHiddenNeurons, maxNumIters).clusterCenters

    val transData = data
      .map(tlp => new TimedLabeledPoint(tlp.time, tlp.label, VectorProjector.projectVector(tlp.features, centres, sigma)))
    val regModel = PerseoLinearRegressionWithOLS.train(transData)

    new PerseoRBFRegressionModel(centres, sigma, regModel.weights, regModel.intercept)

  }

  def train(data: RDD[TimedLabeledPoint], numHiddenNeurons: Int, sigma: Double, maxNumIters: Int): PerseoRBFRegressionModel = {
    // Unsupervised definition of centres in the projected space

    val centres = KMeans.train(data.map(tlp=>tlp.features), numHiddenNeurons, maxNumIters).clusterCenters

    val transData = data
      .map(tlp => new TimedLabeledPoint(tlp.time, tlp.label, VectorProjector.projectVector(tlp.features, centres, sigma)))

    val regModel = PerseoLinearRegressionWithOLS.train(transData)

    new PerseoRBFRegressionModel(centres, sigma, regModel.weights, regModel.intercept)

  }


}
