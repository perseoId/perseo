package com.paradigma.perseo.algorithm.util

import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

object AlgorithmUtil {
  
  def timedLabeledPointToLabeledPoint(input: RDD[TimedLabeledPoint]): RDD[LabeledPoint]={
    val rddlp = input.map { tlp => new LabeledPoint(tlp.label, tlp.features) }
    rddlp
  }
}