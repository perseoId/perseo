package com.paradigma.perseo.algorithm.rbf

import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.VectorProjector

class PerseoRBFClassificationModel(val centres: Array[Vector], val sigma: Double, val weights: Vector, val intercept:
Double, val numFeatures:Int, val numClasses:Int) extends Serializable {

  def predict(input: RDD[Vector]): RDD[Double] = {

    input.mapPartitions { iter =>
      iter.map(v => predict(v))
    }

  }

  def predict(input: Vector): Double = {

    val lrModel = new LogisticRegressionModel(weights, intercept, numFeatures, numClasses)

    val projectedInput = VectorProjector.projectVector(input, centres, sigma)

    lrModel.predict(projectedInput)

  }

}
