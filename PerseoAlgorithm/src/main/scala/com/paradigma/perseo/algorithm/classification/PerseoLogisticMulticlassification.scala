package com.paradigma.perseo.algorithm.classification

import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS

object PerseoLogisticMulticlassification {
  
  def training(input: RDD[TimedLabeledPoint], numClasses: Int): LogisticRegressionModel={
    new LogisticRegressionWithLBFGS().setNumClasses(numClasses).run(input.map { tlp => LabeledPoint(tlp.label, tlp.features) })
  }
}