package com.paradigma.perseo.algorithm.util

import scala.math._

class Complex(val re: Double, val im: Double) {
  def +(value: Complex): Complex = new Complex(re + value.re, im + value.im)
  def -(value: Complex): Complex = new Complex(re - value.re, im - value.im)
  def *(value: Double): Complex = new Complex(re * value, im * value)
  def *(value: Complex): Complex = new Complex(re * value.re - im * value.im, re * value.im + im * value.re)
  def /(value: Double): Complex = new Complex(re / value, im / value)
  def conjugado(value: Complex): Complex = new Complex(re, im * -1)

  override def toString(): String = {
    val a = "%1.3f" format re
    val b = "%1.3f" format abs(im)
    (a, b) match {
      case (_, "0.000") => a
      case ("0.000", _) => b + "i"
      case (_, _) if im > 0 => a + " + " + b + "i"
      case (_, _) => a + " - " + b + "i"
    }
  }
  def module(): Double = Math.sqrt(Math.pow(re, 2) + Math.pow(im, 2))

  def phase(): Double = {
    val phi = Math.atan(im / re)
    val result = (phi > 0) match {
      case true => phi
      case false => phi + 2 * Math.PI
    }
    result
  }

}