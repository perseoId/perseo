package com.paradigma.perseo.algorithm.rbf

import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import com.paradigma.perseo.algorithm.util.VectorProjector
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint

object PerseoRBFClassification {

  def train(data: RDD[TimedLabeledPoint], numHiddenNeurons: Int, sigma: Double, maxNumClass: Int): PerseoRBFClassificationModel = {
    // Unsupervised definition of centres in the projected space
    val maxNumIters = 500

    val centres = KMeans.train(data.map(tlp=>tlp.features), numHiddenNeurons, maxNumIters).clusterCenters

    val transData = data
      .mapPartitions(iter => iter
          .map(lp=>new LabeledPoint(lp.label, VectorProjector.projectVector(lp.features, centres, sigma))))

    val lrModel = new LogisticRegressionWithLBFGS().setNumClasses(maxNumClass).run(transData)

    new PerseoRBFClassificationModel(centres, sigma, lrModel.weights, lrModel.intercept, lrModel.numFeatures, lrModel.numClasses)

  }

  def train(data: RDD[TimedLabeledPoint], numHiddenNeurons: Int, sigma: Double, maxNumClass: Int, maxNumIters: Int): PerseoRBFClassificationModel = {
    // Unsupervised definition of centres in the projected space

    val centres = KMeans.train(data.map(tlp=>tlp.features), numHiddenNeurons, maxNumIters).clusterCenters

    val transData = data
      .mapPartitions(iter => iter
      .map(lp=>new LabeledPoint(lp.label, VectorProjector.projectVector(lp.features, centres, sigma))))

    val lrModel = new LogisticRegressionWithLBFGS().setNumClasses(maxNumClass).run(transData)

    new PerseoRBFClassificationModel(centres, sigma, lrModel.weights, lrModel.intercept, lrModel.numFeatures, lrModel.numClasses)

  }

}
