package com.paradigma.perseo.algorithm.classification

import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import org.apache.spark.mllib.tree.model.GradientBoostedTreesModel
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.tree.GradientBoostedTrees
import org.apache.spark.mllib.regression.LabeledPoint

object PerseoGradientBoostedTrees {
  
  def train(input: RDD[TimedLabeledPoint], boostingStrategy: BoostingStrategy): GradientBoostedTreesModel ={
    GradientBoostedTrees.train(input.map { tlp => LabeledPoint(tlp.label, tlp.features)}, boostingStrategy)
  }
}