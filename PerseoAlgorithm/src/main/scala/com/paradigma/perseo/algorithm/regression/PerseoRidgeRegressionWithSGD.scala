package com.paradigma.perseo.algorithm.regression

import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.util.AlgorithmUtil
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.RidgeRegressionWithSGD
import org.apache.spark.mllib.regression.RidgeRegressionModel

object PerseoRidgeRegressionWithSGD extends RidgeRegressionWithSGD {
  def train(input: RDD[TimedLabeledPoint], numIterations: Int): RidgeRegressionModel={
    RidgeRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double): RidgeRegressionModel={
    RidgeRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam)
  }
  
  def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double, miniBatchFraction:Double): RidgeRegressionModel={
    RidgeRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam, miniBatchFraction)
  }
  
    def train(input: RDD[TimedLabeledPoint], numIterations: Int, stepSize: Double, regParam: Double, miniBatchFraction:Double, initialWeights:Vector): RidgeRegressionModel={
    RidgeRegressionWithSGD.train(AlgorithmUtil.timedLabeledPointToLabeledPoint(input), numIterations, stepSize, regParam, miniBatchFraction, initialWeights)
  }
}