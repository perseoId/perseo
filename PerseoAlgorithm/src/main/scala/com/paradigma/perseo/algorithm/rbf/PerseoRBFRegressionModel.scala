package com.paradigma.perseo.algorithm.rbf

import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LinearRegressionModel
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.VectorProjector


class PerseoRBFRegressionModel(val centres: Array[Vector], val sigma: Double, val weights: Vector, val intercept: Double)
extends Serializable {

  def predict(input: RDD[Vector]): RDD[Double] = {

    val regModel = new LinearRegressionModel(weights, intercept)

    input.mapPartitions { iter =>
      iter.map(v => predict(v))
    }

  }

  def predict(input: Vector): Double = {

    val regModel = new LinearRegressionModel(weights, intercept)
    val projectedInput = VectorProjector.projectVector(input, centres, sigma)
    regModel.predict(projectedInput)

  }

}
