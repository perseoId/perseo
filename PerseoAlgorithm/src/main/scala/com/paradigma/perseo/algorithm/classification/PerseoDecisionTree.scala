package com.paradigma.perseo.algorithm.classification

import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.configuration.Strategy

object PerseoDecisionTree{
  
    def trainClassifier(input: RDD[TimedLabeledPoint], numClasses: Int, categoricalFeaturesInfo: Map[Int, Int], impurity: String, maxDepth: Int, maxBins: Int): DecisionTreeModel ={
    DecisionTree.trainClassifier(input.map { tlp => LabeledPoint(tlp.label, tlp.features) }, numClasses, categoricalFeaturesInfo, impurity, maxDepth, maxBins)
  }
    
}