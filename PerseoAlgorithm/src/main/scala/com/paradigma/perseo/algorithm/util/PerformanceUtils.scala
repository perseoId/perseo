package com.paradigma.perseo.algorithm.util

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.spark.rdd.RDD

object PerformanceUtils {

  def time[R](block: => R): (R, Long) = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    //println("Elapsed time: " + (t1 - t0) + "ns")
    //Thread.sleep(5000)
    (result, (t1 - t0))
  }

  def timeRDD[R](block: => RDD[R]): (RDD[R], Long) = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val c = result.count()
    val t1 = System.nanoTime()
    println("The resulting RDD has " + c + " elements")
    //Thread.sleep(5000)
    (result, (t1 - t0))
  }



  def evalTime[R](block: => R): Long = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    //println("Elapsed time: " + (t1 - t0) + "ns")
    //Thread.sleep(5000)
    (t1 - t0)
  }


  def writeResultsInCSVFile(name: String, header: String, data: Array[String]): Unit = {
    // FileWriter
    //val fileName = "hola"
    val file = new File(name)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(header)
    data.foreach(x => bw.write("\n" + x))
    bw.close()
  }

}
