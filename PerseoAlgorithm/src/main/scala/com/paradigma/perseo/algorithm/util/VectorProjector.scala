package com.paradigma.perseo.algorithm.util

import org.apache.spark.mllib.linalg.{Matrices, Vector, Vectors}
import org.apache.spark.mllib.stat.distribution.MultivariateGaussian

object VectorProjector {

  // sigma is the standard deviation of the Gaussian

  def projectVector(input: Vector, centres: Array[Vector], sigma: Double): Vector = {
    val dim = input.size
    val varArray = Vectors.dense(Array.fill[Double](dim)(sigma*sigma))
    val covMatrix = Matrices.diag(varArray)

    val output = centres.map(mu =>{
      val multGaussian = new MultivariateGaussian(mu, covMatrix)
      multGaussian.pdf(input)
    })

    Vectors.dense(output)

  }

}
