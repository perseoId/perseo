package com.paradigma.perseo.algorithm.util

import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint


class TimedLabeledPoint(val time: Long, override val label: Double, override val features: Vector) extends
LabeledPoint(label, features) {

}
