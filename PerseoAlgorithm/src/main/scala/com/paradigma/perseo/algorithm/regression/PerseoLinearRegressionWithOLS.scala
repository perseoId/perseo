package com.paradigma.perseo.algorithm.regression

import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.distributed.{ CoordinateMatrix, MatrixEntry }
import org.apache.spark.mllib.regression.{ LabeledPoint, LinearRegressionModel }
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.util.MatrixUtils

object PerseoLinearRegressionWithOLS {

  def train(data: RDD[TimedLabeledPoint]): LinearRegressionModel = {

    // X*beta = y
    // X = Q*R, where Q is orthogonal (transpose(Q)*Q=I) and R is un upper triangular matrix with r(i,i) > 0
    // The system to solve can be expressed as follows:
    // X*beta = y => Q*R*beta = y => transpose(Q)*Q*R*beta = transpose(Q)*y => R*beta = z, where z = transpose(Q)*y
    // beta = R^-1*z. It is easier to find R^-1 as it is an upper triangular matrix.

    // data are stored in a CoordinateMatrix (nsamples x dim)

    val indexedXData = data.map(tlp => (new LabeledPoint(tlp.label, Vectors.dense(Array(1.0) ++ tlp.features.toArray)),tlp.time))

    //val indexedXData = xData.zipWithIndex()

    val rddXEntries = indexedXData.flatMap {
      case (lp, row) => {
        val arrayOfEntries = lp.features.toArray.zipWithIndex
          .map { case (value, column) => new MatrixEntry(row, column, value) }
        arrayOfEntries
      }
    }

    val xMatrix = new CoordinateMatrix(rddXEntries)
    val rddYEntries = indexedXData.map { case (lp, row) => new MatrixEntry(row, 0, lp.label) }
    val yMatrix = new CoordinateMatrix(rddYEntries)

    // QR factorization of matrix X
    val qr = xMatrix.toRowMatrix().tallSkinnyQR(false)
    val r = MatrixUtils.convertLocalMatrixToCoordinateMatrix(data.sparkContext, qr.R)
    val pinvr = MatrixUtils.getPseudoInverseMatrix(r.toIndexedRowMatrix()).toCoordinateMatrix()
    val q = MatrixUtils.orderedMatrixProduct(xMatrix, pinvr)
    val z = MatrixUtils.orderedMatrixProduct(q.transpose, yMatrix)
    val beta = MatrixUtils.orderedMatrixProduct(pinvr, z)

    val betaArray = beta.entries.collect()
      .map(entry => (entry.i, entry.value))
      .sortBy(_._1)
      .map { case (row, value) => value }

    val intercept = betaArray(0)
    val weights = betaArray.drop(1)

    new LinearRegressionModel(Vectors.dense(weights), intercept)

  }

}
