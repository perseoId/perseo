package com.paradigma.perseo.algorithm.knn

import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.knn.PerseoKNNRegresionModel

class PerseoKNNRegressionKMeansModel(val neighbourhood: Int, val numClusters: Int, val numIterations: Int) extends PerseoKNNRegresionModel {

  def predict(input: Vector, training: RDD[TimedLabeledPoint]): Double = {

    val kmeansModel = calculateClustersTimed(training)
    val rddClustersResults = training.map { x => (x, kmeansModel.predict(x.features)) }
    val resultKMeansInput=kmeansModel.predict(input)
    val labeledPointPrototipeInput = rddClustersResults
      .filter{case (labeledPoint, result) => result.equals(resultKMeansInput)}
      .map{case (labeledPoint, result) => labeledPoint}
   
    
    val selectedLabels = labeledPointPrototipeInput
      .map(lp => (Vectors.sqdist(lp.features, input), lp.label))
      .sortByKey(true)
      .take(neighbourhood)
      .map{case (distance,value) => value}

    val output = selectedLabels.sum.toDouble/selectedLabels.length.toDouble

    output

  }

  def predict(input: RDD[TimedLabeledPoint], training: RDD[TimedLabeledPoint]): RDD[(TimedLabeledPoint, Double)] ={

    val sc = input.sparkContext
    val kmeansModel = calculateClustersTimed(training)
    val kMeansModelBc = sc.broadcast(kmeansModel)
    val rddResultKMeansTraining = training
      .mapPartitions { iter => iter.map(tlp => (kMeansModelBc.value.predict(tlp.features), tlp)) }

    //val rddResultKMeansTraining = training.map { x => (x, kmeansModel.predict(x.features)) }

    val rddResultKMeansInput=input
      .mapPartitions { iter => iter.map(tlp=> (kMeansModelBc.value.predict(tlp.features), tlp)) }

    // AggregateByKey as an alternative to GroupByKey
    val initialValue = Array.fill[(Double, Double)](neighbourhood)((1.0/0.0, 1.0/0.0)).toList

    val addToSet = (agg: List[(Double, Double)], newValue: ((Double, Double))) => {
      val newList = (agg ::: List(newValue)).zipWithIndex

      // Indexes of newList to be retained: those corresponding to the smallest distances
      val retainIdx = (agg.map{case((dist, label)) => dist} ::: List(newValue._1))
        .zipWithIndex.sortBy(_._1).dropRight(1).map{case(dist, idx) => idx}

      // From newList, the elements with index included in retainIdx must be retained
      val result = newList.filter{case(elem, idx) => retainIdx.contains(idx)}.map{case(elem, idx) => elem}

      result

    }
    val mergePartitionSets = (list1: List[(Double, Double)], list2: List[(Double, Double)]) => {
      (list1 ::: list2).sortBy(_._1).take(neighbourhood)
    }

    val preInputCartesian3 = rddResultKMeansInput
      .join(rddResultKMeansTraining)
      .mapPartitions(iter => iter.map{case(cluster, (i, t)) => ((i.hashCode(), i), (Vectors.sqdist(i.features, t.features), t.label))})
      .aggregateByKey(initialValue)(addToSet, mergePartitionSets)
      .mapPartitions(iter => iter.map{case((hash, itlp), lista) => (itlp, lista.map{case(dist, label) => label})})

        
    val result = preInputCartesian3
      .mapPartitions { iter => iter.map{case(itlp, list) => (itlp, list.sum.toDouble / neighbourhood.toDouble) } }

    result 
    

  }
  
  def calculateClustersTimed(training: RDD[TimedLabeledPoint]): KMeansModel={
    val parsedData = training.map { x => Vectors.dense(x.features.toArray) }.cache()
    val clusters = KMeans.train(parsedData, numClusters, numIterations)
    clusters
  }

}
