package com.paradigma.perseo.training.test

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import java.io.PrintWriter
import scala.io.Source
import scala.tools.nsc.io.File
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.evaluation.RegressionMetrics
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.regression.IsotonicRegression
import org.apache.spark.mllib.regression.RidgeRegressionWithSGD
import org.apache.spark.mllib.regression.LassoWithSGD
import org.apache.spark.mllib.classification.SVMWithSGD

object TestRegressionModelMlib {

  def main(args: Array[String]) {
    
    val sc = new SparkContext(new SparkConf().setMaster("local[2]").setAppName("TestTraining").set("spark.driver.allowMultipleContexts", "true"))
    val pathTest = "/tmp/housepowertest.csv"
    val pathTraining = "/tmp/housepowertraining.csv"
    
    val writeOut = new PrintWriter("/tmp/resultsPredictsmllib.txt", "UTF-8")
    
    val numberOfSamples = new Integer(10)

    val dataTraining = sc.parallelize(pure(pathTraining, numberOfSamples, sc)).map { case ((key, value), stringValues) => (key, value, stringValues.split("@").map { value => value.toDouble }) }
    
    
    val rddFeaturesTraining = dataTraining.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeaturesTraining)
    val rddLabeledPointTraining: RDD[TimedLabeledPoint] = dataTraining.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }    
    
    val ridgeRegressionModelSgd = RidgeRegressionWithSGD.train(rddLabeledPointTraining.map { tlp => LabeledPoint(tlp.label, tlp.features) }, 1000, 0.05, 0.01)
    val LassoRegressionModelSgd = LassoWithSGD.train(rddLabeledPointTraining.map { tlp => new LabeledPoint(tlp.label, tlp.features) }, 1000, 0.05, 0.01)
    
    val dataTest = sc.parallelize(pure(pathTest, numberOfSamples, sc)).map { case ((key, value), stringValues) => (key, value, stringValues.split("@").map { value => value.toDouble }) }
    val rddLabeledPointTest: RDD[TimedLabeledPoint] = dataTest.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }
    
    val predictionAndLabelRidge = rddLabeledPointTest.map { lp => (ridgeRegressionModelSgd.predict(lp.features), lp.label) }
    val regresionMetricsRidge = new RegressionMetrics(predictionAndLabelRidge)
    
    val predictionAndLabelLasso = rddLabeledPointTest.map { lp => (LassoRegressionModelSgd.predict(lp.features), lp.label) }
    val regresionMetricsLasso = new RegressionMetrics(predictionAndLabelLasso)
    
    
    writeOut.println("RIDGE RESULTS METRICS")
    writeOut.println("explainedVariance: "+regresionMetricsRidge.explainedVariance)
    writeOut.println("meanAbsoluteError: "+regresionMetricsRidge.meanAbsoluteError)
    writeOut.println("meanSquaredError: "+regresionMetricsRidge.meanSquaredError)
    writeOut.println("r2: "+regresionMetricsRidge.r2)
    writeOut.println("rootMeanSquaredError: "+regresionMetricsRidge.rootMeanSquaredError)
    
        
    writeOut.println("LASSO RESULTS METRICS")
    writeOut.println("explainedVariance: "+regresionMetricsLasso.explainedVariance)
    writeOut.println("meanAbsoluteError: "+regresionMetricsLasso.meanAbsoluteError)
    writeOut.println("meanSquaredError: "+regresionMetricsLasso.meanSquaredError)
    writeOut.println("r2: "+regresionMetricsLasso.r2)
    writeOut.println("rootMeanSquaredError: "+regresionMetricsLasso.rootMeanSquaredError)
    
    writeOut.close()
    


  }
  private def pure(path: String, numberOfSamples: Int, sc: SparkContext): Array[((Long, Double), String)] = {
    val bufferedSource = Source.fromFile(path)
    val arrayFile = sc.textFile(path).collect()
    var rows = scala.collection.mutable.ArrayBuffer.empty[Array[String]]

    for (line <- arrayFile) {
      rows += line.split(',').map(_.trim)
    }

    val dataArray = rows.map(row => (row.head.toLong, row.last.toDouble)).reverse.sliding(numberOfSamples + 1).map({
      case list => ((list.head._1, list.head._2), list.drop(1).toList.map({ case (timestamp, value) => value }))
    }).filter { case (key, listValue) => listValue.size == numberOfSamples }.map { case (key, listValue) => (key, transformListInString(listValue)) }.toArray.sortBy(_._1)
    //dataArray.foreach(println)
    //prueba
    dataArray
  }
    private def transformListInString(listValue: List[Double]): String = {
    var stringReturn = ""
    for (i <- 0 to listValue.size - 1) {

      if (i == 0)
        stringReturn = stringReturn + listValue.apply(i).toString()
      else
        stringReturn = stringReturn + "@" + listValue.apply(i).toString()

    }
    stringReturn
  }

  private def normalizeData(rddTrainingData: RDD[(Int, Double, Array[Double])], saveMode: String, sqlContext: org.apache.spark.sql.SQLContext, pathHdfs: String): RDD[LabeledPoint] = {
    val rddFeatures = rddTrainingData.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeatures)

    rddTrainingData.map { case (key, target, features) => LabeledPoint(target, scalerModel.transform(Vectors.dense(features))) }
  }

}