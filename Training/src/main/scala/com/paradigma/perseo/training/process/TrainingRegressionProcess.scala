package com.paradigma.perseo.training.process

import scala.io.Source
import org.apache.spark.SparkContext
import com.paradigma.perseo.training.util.SparkConfFactory
import com.datastax.spark.connector._
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.Vector
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.sql.SaveMode
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithSGD
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithOLS
import com.paradigma.perseo.algorithm.regression.PerseoLassoRegressionWithSGD
import com.paradigma.perseo.algorithm.regression.PerseoRidgeRegressionWithSGD
import com.paradigma.perseo.algorithm.rbf.PerseoRBFRegression

object TrainingRegressionProcess {

  def training(algorithm: String) {
    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)

    val rddTransformTrainingData: RDD[TimedLabeledPoint] = loadDataCassandra(sc, algorithm)

    if (algorithm.contains("linearregression")) {
      val linearRegressionModel = if (algorithm.contains("sgd")) PerseoLinearRegressionWithSGD.train(rddTransformTrainingData, 5000, 0.01)
      else PerseoLinearRegressionWithOLS.train(rddTransformTrainingData)

      //save data modelLogisticRegresion in cassandra
      CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
        session.execute("USE perseo;")
        session.execute("CREATE TABLE IF NOT EXISTS modelRegression(timetraining bigint, typemodel text, intercept double, weights text, sigma double, centres text, PRIMARY KEY(typemodel, timetraining));")
        if (algorithm.contains("sgd"))
          session.execute("INSERT INTO modelRegression (timetraining, typemodel, intercept, weights) values(" + System.currentTimeMillis().toInt + ",'SGD'," + linearRegressionModel.intercept + ",'" + linearRegressionModel.weights.toArray.mkString("@") + "')");
        else if (algorithm.contains("ols"))
          session.execute("INSERT INTO modelRegression (timetraining, typemodel, intercept, weights) values(" + System.currentTimeMillis().toInt + ", 'OLS'," + linearRegressionModel.intercept + ",'" + linearRegressionModel.weights.toArray.mkString("@") + "')");
        session.close();

      }

    }else if (algorithm.contains("lasso")){
        val lassoModel = PerseoLassoRegressionWithSGD.train(rddTransformTrainingData, 5000, 0.05, 0.01)
        CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
        session.execute("USE perseo;")
        session.execute("CREATE TABLE IF NOT EXISTS modelRegression(timetraining bigint, typemodel text, intercept double, weights text, sigma double, centres text, PRIMARY KEY(typemodel, timetraining));")
        session.execute("INSERT INTO modelRegression (timetraining, typemodel, intercept, weights) values(" + System.currentTimeMillis().toInt + ", 'LASSO'," + lassoModel.intercept + ",'" + lassoModel.weights.toArray.mkString("@") + "')");
        session.close();
      }
    }else if (algorithm.contains("ridge")){
      val ridgeModel = PerseoRidgeRegressionWithSGD.train(rddTransformTrainingData, 5000, 0.05, 0.01)
      CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
        session.execute("USE perseo;")
        session.execute("CREATE TABLE IF NOT EXISTS modelRegression(timetraining bigint, typemodel text, intercept double, weights text, sigma double, centres text, PRIMARY KEY(typemodel, timetraining));")
        session.execute("INSERT INTO modelRegression (timetraining, typemodel, intercept, weights) values(" + System.currentTimeMillis().toInt + ", 'RIDGE'," + ridgeModel.intercept + ",'" + ridgeModel.weights.toArray.mkString("@") + "')");
        session.close();
      }
    }else if (algorithm.contains("rbf")) {
      val rbfModel=PerseoRBFRegression.train(rddTransformTrainingData, 100, 1.0)
      //save data rbfModel in cassandra
      
      CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
        session.execute("USE perseo;")
        session.execute("CREATE TABLE IF NOT EXISTS modelRegression(timetraining bigint, typemodel text, intercept double, weights text, sigma double, centres text, PRIMARY KEY(typemodel, timetraining));")
        session.execute("INSERT INTO modelRegression (timetraining, typemodel, intercept, weights, sigma, centres) values(" + System.currentTimeMillis().toInt+", 'RBF'," +rbfModel.intercept + ",'" + rbfModel.weights.toArray.mkString("@") + "', "+rbfModel.sigma+", '"+rbfModel.centres.mkString("@")+"')");
        session.close();

      }
      
    }

  }

  private def loadDataCassandra(sc: SparkContext, algorithm: String): RDD[TimedLabeledPoint] = {
    val rddTrainingData = sc.cassandraTable("perseo", "datatrainingregression").map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore").split("@").map { value => value.toDouble }) }
    normalizeData(rddTrainingData, algorithm)
  }

  private def normalizeData(rddTrainingData: RDD[(Int, Double, Array[Double])], algorithm: String): RDD[TimedLabeledPoint] = {
    val rddFeatures = rddTrainingData.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeatures)
    //save normalizador
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE perseo;")
      session.execute("CREATE TABLE IF NOT EXISTS scalerModel(algorithm text, timemodel bigint, std text, mean text, withStd boolean, withMean boolean, PRIMARY KEY(algorithm, timemodel));")
      session.execute("INSERT INTO scalerModel (algorithm, timemodel, std, mean, withStd, withMean) values('" + algorithm + "'," + System.currentTimeMillis().toInt + ", '" + scalerModel.std.toArray.mkString("@") + "','" + scalerModel.mean.toArray.mkString("@") + "'," + scalerModel.withStd + "," + scalerModel.withMean + ")")
      session.close();
    }

    rddTrainingData.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }
  }

}