package com.paradigma.perseo.training.process

import org.apache.spark.SparkContext
import com.paradigma.perseo.training.util.SparkConfFactory
import com.datastax.spark.connector._
import com.paradigma.perseo.algorithm.util.Complex
import com.paradigma.perseo.algorithm.util.FFT
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.classification.PerseoDecisionTree
import com.paradigma.perseo.algorithm.classification.PerseoGradientBoostedTrees
import com.paradigma.perseo.algorithm.classification.PerseoLogisticMulticlassification
import com.paradigma.perseo.algorithm.classification.PerseoNaiveBayes
import com.paradigma.perseo.algorithm.classification.PerseoRandomForest
import scala.util.Random

object TrainingClassificationProcess {

  def training(algorithm: String) {
    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    val rddTrainingData = sc.cassandraTable("perseo", "datatrainingclassification")
    
    //val path = "hdfs://stratio/user/stratio/perseo/modelclass/"
    val path = "/tmp/modelclass/"

    /*
     * features X, Y, Z
     * mean and Std of the normal data
     * from fft module:
     * max, freq to max
     * bw to 50% and 95%
     * 
     */

    val numClasses = 7

    val frequencyInSec = 5
    val intervalInSec = 10
    val samples = frequencyInSec * intervalInSec

    val longFft = FFT.convertPow2(samples)

    val vectorFrequency = (-frequencyInSec / 2.0 to frequencyInSec / 2.0).by(frequencyInSec.toDouble / (longFft - 1)).toArray.takeRight(longFft / 2)

    val rddDataX = rddTrainingData.map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore").split("@").map { x => new Complex(x.split(",").apply(0).toDouble, 0) }.toList) }
    val rddDataY = rddTrainingData.map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore").split("@").map { x => new Complex(x.split(",").apply(1).toDouble, 0) }.toList) }
    val rddDataZ = rddTrainingData.map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore").split("@").map { x => new Complex(x.split(",").apply(2).toDouble, 0) }.toList) }

    val rddTimeLabel = rddTrainingData.map { row => (row.getInt("keytime"), (row.getDouble("valuekey")-1)) }

    val dataFttModuleX = rddDataX.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
    val dataFttModuleY = rddDataY.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
    val dataFttModuleZ = rddDataZ.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }

    val meanAndStdX = rddDataX.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }
    val meanAndStdY = rddDataY.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }
    val meanAndStdZ = rddDataZ.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }

    val maxAndPosFreqX = dataFttModuleX.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }
    val maxAndPosFreqY = dataFttModuleY.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }
    val maxAndPosFreqZ = dataFttModuleZ.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqX = dataFttModuleX.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqY = dataFttModuleY.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqZ = dataFttModuleZ.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    // features mean, std, max, posFreq max, posFreq bw50, posFreq bw95
    val featuresX = meanAndStdX.join(maxAndPosFreqX).join(bandwidth50And95PosFreqX)
    val featuresY = meanAndStdY.join(maxAndPosFreqY).join(bandwidth50And95PosFreqY)
    val featuresZ = meanAndStdZ.join(maxAndPosFreqZ).join(bandwidth50And95PosFreqZ)

    val trainingData = rddTimeLabel.join(featuresX).join(featuresY).join(featuresZ).map {
      case (timestamp, (((label, (((meanX, stdX), (maxX, pfreqX)), (bw50X, bx95X))), (((meanY, stdY), (maxY, pfreqY)), (bw50Y, bw95Y))), (((meanZ, stdZ), (maxZ, pfreqZ)), (bw50Z, bw95Z)))) =>

        val arrayValues = Array(meanX, stdX, maxX, pfreqX, bw50X, bx95X, meanY, stdY, maxY, pfreqY, bw50Y, bw95Y, meanZ, stdZ, maxZ, pfreqZ, bw50Z, bw95Z)
        val features = Vectors.dense(arrayValues)
        new TimedLabeledPoint(timestamp, label, features)
    }
    
    //algorithm: (decisionTree, gradientBoostedTree, logisticMulticlass, naiveBayes or randomForest)
    if (algorithm.equals("decisiontree")) {
      
      // Train a DecisionTree model.
      //  Empty categoricalFeaturesInfo indicates all features are continuous.

      val categoricalFeaturesInfo = Map[Int, Int]()
      val impurity = "gini"
      val maxDepth = 16
      val maxBins = 32
      val modelTraning = PerseoDecisionTree.trainClassifier(trainingData, numClasses, categoricalFeaturesInfo,
        impurity, maxDepth, maxBins)
      modelTraning.save(sc, path+"decisiontree")
   
    } else if (algorithm.equals("logisticmulticlass")) {
      
      val modelTraning = PerseoLogisticMulticlassification.training(trainingData, numClasses)
      modelTraning.save(sc, path+"logisticmulticlass")

    } else if (algorithm.equals("naivebayes")) {
      
      val lambda = 1.0
      val modelType = "multinomial"
      val modelTraning = PerseoNaiveBayes.train(trainingData, lambda, modelType)
      println("save naiveBayes model")
      modelTraning.save(sc, path+"naivebayes")

    } else if (algorithm.equals("randomforest")) {
      
      // Train a RandomForest model.
      //  Empty categoricalFeaturesInfo indicates all features are continuous.
      val categoricalFeaturesInfo = Map[Int, Int]()
      val numTrees = 21 // Use more in practice.
      val featureSubsetStrategy = "auto" // Let the algorithm choose.
      val impurity = "gini"
      val maxDepth = 16
      val maxBins = 32
      val modelTraning = PerseoRandomForest.trainClassifier(trainingData, numClasses, categoricalFeaturesInfo, numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins, Random.nextInt())
      modelTraning.save(sc, path+"randomforest")

    } 

  }

  def calculateMean(listComplex: List[Complex]): Double = {
    listComplex.map(_.re).sum / listComplex.length
  }

  def calculateStd(listComplex: List[Complex]): Double = {
    val mean = calculateMean(listComplex)
    Math.sqrt(listComplex.map(_.re).map { x => (x - mean) * (x - mean) }.sum) / listComplex.length
  }

  def calculatePointMax(max: Double, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    vectorFrequency.apply(listModuleComplex.indexOf(max))
  }

  def caculateFreqPositionBandwidth(percentage: Int, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    val totalPercentageBandwidth = (listModuleComplex.sum * percentage) / 100.0
    var sumBandwidth = 0.0
    var position = -1
    for (i <- 0 to listModuleComplex.size - 1) {
      sumBandwidth += listModuleComplex.apply(i)

      if (position == -1) {
        if (sumBandwidth >= totalPercentageBandwidth) {
          position = i
        }
      }
    }
    vectorFrequency.apply(position)
  }
}