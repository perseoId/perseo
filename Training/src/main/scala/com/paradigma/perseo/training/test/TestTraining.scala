package com.paradigma.perseo.training.test

import scala.io.Source
import scala.tools.nsc.io.File
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.evaluation.RegressionMetrics
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithSGD
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithOLS
import com.paradigma.perseo.algorithm.knn.PerseoKNNRegresionModel
import com.paradigma.perseo.algorithm.knn.PerseoKNNRegressionKMeansModel
import java.io.PrintWriter
import com.paradigma.perseo.algorithm.rbf.PerseoRBFRegression

object TestTraining {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf().setMaster("local[2]").setAppName("TestTraining").set("spark.driver.allowMultipleContexts", "true"))

    val pathTest = "/tmp/housepowertest.csv"
    val pathTraining = "/tmp/housepowertraining.csv"
    
    val writeOut = new PrintWriter("/tmp/resultsPredictsAlgorithm.txt", "UTF-8")

    val numberOfSamples = new Integer(10)

    val dataTraining = sc.parallelize(pure(pathTraining, numberOfSamples, sc)).map { case ((key, value), stringValues) => (key, value, stringValues.split("@").map { value => value.toDouble }) }

    val rddFeaturesTraining = dataTraining.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeaturesTraining)
    val rddLabeledPointTraining: RDD[TimedLabeledPoint] = dataTraining.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }

    val linearRegressionModelSgd = PerseoLinearRegressionWithSGD.train(rddLabeledPointTraining, 1000, 0.05)
    val linearRegressionModelOds = PerseoLinearRegressionWithOLS.train(rddLabeledPointTraining)
    val knnModel = new PerseoKNNRegressionKMeansModel(20, 500, 100)
    val rbfModel = PerseoRBFRegression.train(rddLabeledPointTraining, 100, 1.0)
    
    val dataTest = sc.parallelize(pure(pathTest, numberOfSamples, sc)).map { case ((key, value), stringValues) => (key, value, stringValues.split("@").map { value => value.toDouble }) }
    val rddLabeledPointTest: RDD[TimedLabeledPoint] = dataTest.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }

    val predictionAndLabelLinearSgd = rddLabeledPointTest.map { lp => (linearRegressionModelSgd.predict(lp.features), lp.label) }
    val regresionMetricsSgd = new RegressionMetrics(predictionAndLabelLinearSgd)
    
    val predictionAndLabelLinearOds = rddLabeledPointTest.map { lp => (linearRegressionModelOds.predict(lp.features), lp.label) }
    val regresionMetricsOds = new RegressionMetrics(predictionAndLabelLinearOds)
    
    val predictionAndLabelKnn = knnModel.predict(rddLabeledPointTest, rddLabeledPointTraining).map { case (tlp, pre) => (pre, tlp.label) }.filter{case (tlp, pre) => !tlp.isInfinity}
    predictionAndLabelKnn.foreach(println)
    val regresionMetricsKnn = new RegressionMetrics(predictionAndLabelKnn)
    
    println("KNN (neighbourhood, clusters, iterations) (20, 500, 100)---->RESULTS.")
    println("meanAbsoluteError: " + regresionMetricsKnn.meanAbsoluteError)
    println("meanSquaredError: " + regresionMetricsKnn.meanSquaredError)
    println("r2: " + regresionMetricsKnn.r2)
    println("explainedVariance: " + regresionMetricsKnn.explainedVariance)
    println("rootMeanSquaredError: " + regresionMetricsKnn.rootMeanSquaredError)
    
    val predictionAndLabelRbf = rddLabeledPointTest.map { lp => (rbfModel.predict(lp.features), lp.label) }
    val regresionMetricsRbf = new RegressionMetrics(predictionAndLabelRbf)

    writeOut.write("-----------START------------\n")
    writeOut.write("SGD (numIterations, stepSize) (1000, 0.05)---->RESULTS.\n")
    writeOut.write("meanAbsoluteError: " + regresionMetricsSgd.meanAbsoluteError+"\n")
    writeOut.write("meanSquaredError: " + regresionMetricsSgd.meanSquaredError+"\n")
    writeOut.write("r2: " + regresionMetricsSgd.r2+"\n")
    writeOut.write("explainedVariance: " + regresionMetricsSgd.explainedVariance+"\n")
    writeOut.write("rootMeanSquaredError: " + regresionMetricsSgd.rootMeanSquaredError+"\n")
    writeOut.write("\n")
    writeOut.write("ODS---->RESULTS.")
    writeOut.write("meanAbsoluteError: " + regresionMetricsOds.meanAbsoluteError+"\n")
    writeOut.write("meanSquaredError: " + regresionMetricsOds.meanSquaredError+"\n")
    writeOut.write("r2: " + regresionMetricsOds.r2+"\n")
    writeOut.write("explainedVariance: " + regresionMetricsOds.explainedVariance+"\n")
    writeOut.write("rootMeanSquaredError: " + regresionMetricsOds.rootMeanSquaredError+"\n")
    writeOut.write("\n")
    writeOut.write("KNN (neighbourhood, clusters, iterations) (20, 500, 10)---->RESULTS.\n")
    writeOut.write("meanAbsoluteError: " + regresionMetricsKnn.meanAbsoluteError+"\n")
    writeOut.write("meanSquaredError: " + regresionMetricsKnn.meanSquaredError+"\n")
    writeOut.write("r2: " + regresionMetricsKnn.r2)
    writeOut.write("explainedVariance: " + regresionMetricsKnn.explainedVariance+"\n")
    writeOut.write("rootMeanSquaredError: " + regresionMetricsKnn.rootMeanSquaredError+"\n")
    writeOut.write("\n")
    writeOut.write("RBF (numHiden, sigma) (100, 1.0)---->RESULTS.\n")
    writeOut.write("meanAbsoluteError: " + regresionMetricsRbf.meanAbsoluteError+"\n")
    writeOut.write("meanSquaredError: " + regresionMetricsRbf.meanSquaredError+"\n")
    writeOut.write("r2: " + regresionMetricsRbf.r2)
    writeOut.write("explainedVariance: " + regresionMetricsRbf.explainedVariance+"\n")
    writeOut.write("rootMeanSquaredError: " + regresionMetricsRbf.rootMeanSquaredError+"\n")
    
    writeOut.close()

  }

  private def pure(path: String, numberOfSamples: Int, sc: SparkContext): Array[((Long, Double), String)] = {
    val bufferedSource = Source.fromFile(path)
    val arrayFile = sc.textFile(path).collect()
    var rows = scala.collection.mutable.ArrayBuffer.empty[Array[String]]

    for (line <- arrayFile) {
      rows += line.split(',').map(_.trim)
    }

    val dataArray = rows.map(row => (row.head.toLong, row.last.toDouble)).reverse.sliding(numberOfSamples + 1).map({
      case list => ((list.head._1, list.head._2), list.drop(1).toList.map({ case (timestamp, value) => value }))
    }).filter { case (key, listValue) => listValue.size == numberOfSamples }.map { case (key, listValue) => (key, transformListInString(listValue)) }.toArray.sortBy(_._1)
    //dataArray.foreach(println)
    //prueba
    dataArray
  }

  private def transformListInString(listValue: List[Double]): String = {
    var stringReturn = ""
    for (i <- 0 to listValue.size - 1) {

      if (i == 0)
        stringReturn = stringReturn + listValue.apply(i).toString()
      else
        stringReturn = stringReturn + "@" + listValue.apply(i).toString()

    }
    stringReturn
  }

  private def normalizeData(rddTrainingData: RDD[(Int, Double, Array[Double])], saveMode: String, sqlContext: org.apache.spark.sql.SQLContext, pathHdfs: String): RDD[LabeledPoint] = {
    val rddFeatures = rddTrainingData.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeatures)

    rddTrainingData.map { case (key, target, features) => LabeledPoint(target, scalerModel.transform(Vectors.dense(features))) }
  }

}