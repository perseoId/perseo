package com.paradigma.perseo.training.process

object TrainingMain {
  def main(args: Array[String]) {
    if (args.length != 2) {
      System.err.println(
        s"""
           |Usage: TrainingProcess <type> <algorithm> 
           |  <type> is the form to predict. classification | regression
           |  <algorithm> is regression -> (linearRegression-sgd, linearRegression-ols, ridge, lasso or rbf). Classification -> (decisionTree, logisticMulticlass, naiveBayes or randomForest) 
           |
        """.stripMargin)
      System.exit(1)
    } else {
      if (!args.apply(0).toString().toLowerCase().equals("regression") && !args.apply(0).toString().toLowerCase().equals("classification")) {
        System.err.println("<type> is not valid: " + args.apply(0).toString().toLowerCase() + ". Must choose classification|regression")
        System.exit(1)
      }
      if (args.apply(0).toString().toLowerCase().equals("regression")) {
        if (!args.apply(1).toString().toLowerCase().equals("linearregression-ols") && !args.apply(1).toString().toLowerCase().equals("linearregression-sgd") && !args.apply(1).toString().toLowerCase().equals("rbf") && !args.apply(1).toString().toLowerCase().equals("lasso") && !args.apply(1).toString().toLowerCase().equals("ridge")) {
          System.err.println("<algorithm> is not valid: " + args.apply(1).toString().toLowerCase() + ". When you choose regression you must choose linearRegression, ridge, lasso or rbf")
          System.exit(1)
        } else {
          println("init regression")
          TrainingRegressionProcess.training(args.apply(1).toString().toLowerCase())
        }
      } else if (args.apply(0).toString().toLowerCase().equals("classification")) {
        if (!args.apply(1).toString().toLowerCase().equals("decisiontree") && !args.apply(1).toString().toLowerCase().equals("logisticmulticlass") && !args.apply(1).toString().toLowerCase().equals("randomforest") && !args.apply(1).toString().toLowerCase().equals("naivebayes")) {
          System.err.println("<algorithm> is not valid: " + args.apply(1).toString().toLowerCase() + ". When you choose classification you must choose decisionTree, logisticMulticlass, naiveBayes or randomForest")
          System.exit(1)
        } else {
          println("init classification")
          TrainingClassificationProcess.training(args.apply(1).toString().toLowerCase())
        }

      }
    }
  }
}