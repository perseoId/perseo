package com.paradigma.perseo.training.process

import org.apache.spark.SparkContext
import com.paradigma.perseo.training.util.SparkConfFactory
import com.datastax.spark.connector._
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.Vectors
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithSGD
import com.paradigma.perseo.algorithm.regression.PerseoLinearRegressionWithOLS
import com.paradigma.perseo.algorithm.rbf.PerseoRBFRegression
import com.paradigma.perseo.algorithm.regression.PerseoLassoRegressionWithSGD
import com.paradigma.perseo.algorithm.regression.PerseoRidgeRegressionWithSGD
import org.apache.spark.mllib.evaluation.RegressionMetrics
import com.datastax.spark.connector.cql.CassandraConnector

object RegressionResults {
  def main(args: Array[String]) {
    executeRegressionResults()
  }
  def executeRegressionResults() {

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    val rddCassandra = sc.cassandraTable("perseo", "datatrainingregression")
    
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("CREATE KEYSPACE IF NOT EXISTS perseo WITH replication = {'class': 'SimpleStrategy','replication_factor': '1'};")
      session.execute("USE perseo;")
      session.execute("DROP TABLE IF EXISTS regressionresults;")
      session.execute("CREATE TABLE IF NOT EXISTS regressionresults(keyString text, algorithm text, valuekey Double, PRIMARY KEY(keyString, algorithm));")

      session.close();
    }
    
    val rddData = rddCassandra.map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore").split("@").map { value => value.toDouble }) }

    val Array(dataTrainingRDD, dataTestRDD) = rddData.randomSplit(Array(0.7, 0.3))
    val rddFeatures = dataTrainingRDD.map { case (key, target, features) => Vectors.dense(features) }
    val scaler = new StandardScaler()
    val scalerModel = scaler.fit(rddFeatures)
    
    val dataTrainingNormalize=dataTrainingRDD.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }
    val dataTestNormalize=dataTestRDD.map { case (key, target, features) => new TimedLabeledPoint(key, target, scalerModel.transform(Vectors.dense(features))) }
    
    //Lasso
    val lassoModel=PerseoLassoRegressionWithSGD.train(dataTrainingNormalize, 5000, 0.05, 0.01)
    val predLabelLasso = dataTestNormalize.map { lp => (lassoModel.predict(lp.features), lp.label) }
    val metricsLasso = new RegressionMetrics(predLabelLasso)

    
    //Ridge
    val ridgeModel=PerseoRidgeRegressionWithSGD.train(dataTrainingNormalize, 5000, 0.05, 0.01)
    val predLabelRidge = dataTestNormalize.map { lp => (ridgeModel.predict(lp.features), lp.label) }
    val metricsRidge = new RegressionMetrics(predLabelRidge)

    
    //sgd
    val linearRegressionModelSgd = PerseoLinearRegressionWithSGD.train(dataTrainingNormalize, 5000, 0.01)
    val predLabelSGD = dataTestNormalize.map { lp => (linearRegressionModelSgd.predict(lp.features), lp.label) }
    val metricsSGD = new RegressionMetrics(predLabelSGD)
    
    //ols
    val linearRegressionModelOls = PerseoLinearRegressionWithOLS.train(dataTrainingNormalize)
    val predLabelOLS = dataTestNormalize.map { lp => (linearRegressionModelOls.predict(lp.features), lp.label) }
    val metricsOLS = new RegressionMetrics(predLabelOLS)

    //RBF
    val rbfModel = PerseoRBFRegression.train(dataTrainingNormalize, 100, 1.0)
    val predLabelRBF = dataTestNormalize.map { lp => (rbfModel.predict(lp.features), lp.label) }
    val metricsRBF = new RegressionMetrics(predLabelRBF)   
    
    println("--RESULTS--")
    println("Lasso: "+metricsLasso.meanAbsoluteError)
    println("Ridge: "+metricsRidge.meanAbsoluteError)
    println("SGD: "+metricsSGD.meanAbsoluteError)
    println("OLS: "+metricsOLS.meanAbsoluteError)
    println("RBF: "+metricsRBF.meanAbsoluteError)
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE perseo;")
      //Lasso
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('lasso', 'meanAbsoluteError', "+metricsLasso.meanAbsoluteError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('lasso', 'explainedVariance', "+metricsLasso.explainedVariance+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('lasso', 'meanSquaredError', "+metricsLasso.meanSquaredError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('lasso', 'r2', "+metricsLasso.r2+")")
      //Ridge
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ridge', 'meanAbsoluteError', "+metricsRidge.meanAbsoluteError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ridge', 'explainedVariance', "+metricsRidge.explainedVariance+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ridge', 'meanSquaredError', "+metricsRidge.meanSquaredError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ridge', 'r2', "+metricsRidge.r2+")")
      //sgd
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('sgd', 'meanAbsoluteError', "+metricsSGD.meanAbsoluteError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('sgd', 'explainedVariance', "+metricsSGD.explainedVariance+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('sgd', 'meanSquaredError', "+metricsSGD.meanSquaredError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('sgd', 'r2', "+metricsSGD.r2+")")
      //ols
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ols', 'meanAbsoluteError', "+metricsOLS.meanAbsoluteError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ols', 'explainedVariance', "+metricsOLS.explainedVariance+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ols', 'meanSquaredError', "+metricsOLS.meanSquaredError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('ols', 'r2', "+metricsOLS.r2+")")
      //rbf
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('rbf', 'meanAbsoluteError', "+metricsRBF.meanAbsoluteError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('rbf', 'explainedVariance', "+metricsRBF.explainedVariance+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('rbf', 'meanSquaredError', "+metricsRBF.meanSquaredError+")")
      session.execute("INSERT INTO regressionresults(algorithm, keyString, valuekey) values('rbf', 'r2', "+metricsRBF.r2+")")

      session.close();
    }
    
    
  }
  
}