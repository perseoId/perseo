package com.paradigma.perseo.training.test

import org.apache.spark.SparkContext
import com.paradigma.perseo.training.util.SparkConfFactory
import com.datastax.spark.connector._
import com.paradigma.perseo.algorithm.util.Complex
import com.paradigma.perseo.algorithm.util.FFT
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import com.paradigma.perseo.algorithm.util.TimedLabeledPoint
import com.paradigma.perseo.algorithm.classification.PerseoDecisionTree
import com.paradigma.perseo.algorithm.classification.PerseoGradientBoostedTrees
import com.paradigma.perseo.algorithm.classification.PerseoLogisticMulticlassification
import com.paradigma.perseo.algorithm.classification.PerseoNaiveBayes
import com.paradigma.perseo.algorithm.classification.PerseoRandomForest
import scala.util.Random
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.mllib.classification.NaiveBayes
import org.apache.spark.mllib.classification.NaiveBayesModel

object ClassificationTest {
  def main(args: Array[String]) {

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    val rddCassandra = sc.cassandraTable("perseo", "datatrainingclassification")
    val mode = "prediction"
    val algorithm="logisticmulticlass"
    val path="/tmp/"

    val rddData = rddCassandra.map { row => (row.getInt("keytime"), row.getDouble("valuekey"), row.getString("valuesbefore")) }
    val type1Data = rddData.filter { x => x._2 == 1.0 }.collect()
    val type2Data = rddData.filter { x => x._2 == 2.0 }.collect()
    val type3Data = rddData.filter { x => x._2 == 3.0 }.collect()
    val type4Data = rddData.filter { x => x._2 == 4.0 }.collect()
    val type5Data = rddData.filter { x => x._2 == 5.0 }.collect()
    val type6Data = rddData.filter { x => x._2 == 6.0 }.collect()
    val type7Data = rddData.filter { x => x._2 == 7.0 }.collect()

    val rddTrainingData = sc.parallelize(type1Data.slice(0, 700).++:(type2Data.slice(0, 700)).++:(type3Data.slice(0, 700)).++:(type4Data.slice(0, 700)).++:(type5Data.slice(0, 700)).++:(type6Data.slice(0, 700)).++:(type7Data.slice(0, 700)).toSeq)
    val rddTestData = sc.parallelize(type1Data.slice(701, 2500).++:(type2Data.slice(701, type2Data.length - 1)).++:(type3Data.slice(701, 2500)).++:(type4Data.slice(701, 2500)).++:(type5Data.slice(701, 2500)).++:(type6Data.slice(701, 2500)).++:(type7Data.slice(701, 2500)).toSeq)

    /*
     * features X, Y, Z
     * mean and Std of the normal data
     * from fft module:
     * max, freq to max
     * bw to 50% and 95%
     * 
     */

    val numClasses = 7

    if (mode.equals("training")) {
      val trainingData = calculateFeatures(rddTrainingData)
      
      if (algorithm.equals("decisiontree")) {
      
      // Train a DecisionTree model.
      //  Empty categoricalFeaturesInfo indicates all features are continuous.

      val categoricalFeaturesInfo = Map[Int, Int]()
      val impurity = "gini"
      val maxDepth = 16
      val maxBins = 32
      val modelTraning = PerseoDecisionTree.trainClassifier(trainingData, numClasses, categoricalFeaturesInfo,
        impurity, maxDepth, maxBins)
      modelTraning.save(sc, path+"decisiontree")
   
    } else if (algorithm.equals("logisticmulticlass")) {
      
      val modelTraning = PerseoLogisticMulticlassification.training(trainingData, numClasses)
      modelTraning.save(sc, path+"logisticmulticlass")

    } else if (algorithm.equals("naivebayes")) {
      
      val lambda = 1.0
      val modelType = "multinomial"
      val modelTraning = PerseoNaiveBayes.train(trainingData, lambda, modelType)
      println("save naiveBayes model")
      modelTraning.save(sc, path+"naivebayes")

    } else if (algorithm.equals("randomforest")) {
      
      // Train a RandomForest model.
      //  Empty categoricalFeaturesInfo indicates all features are continuous.
      val categoricalFeaturesInfo = Map[Int, Int]()
      val numTrees = 21 // Use more in practice.
      val featureSubsetStrategy = "auto" // Let the algorithm choose.
      val impurity = "gini"
      val maxDepth = 16
      val maxBins = 32
      val modelTraning = PerseoRandomForest.trainClassifier(trainingData, numClasses, categoricalFeaturesInfo, numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins, Random.nextInt())
      modelTraning.save(sc, path+"randomforest")

    } 

    } else if (mode.equals("prediction")) {
      val predictionsAndLabel:RDD[(Double, Double)]=if (algorithm.equals("decisiontree")) {
      val model = DecisionTreeModel.load(sc, path+"decisiontree")
      val testData = calculateFeatures(rddTestData)
      val predictionsAndLabel = testData.map { point =>
        val prediction = model.predict(point.features)
        (prediction, point.label)
      }
      predictionsAndLabel
      
   
    } else if (algorithm.equals("logisticmulticlass")) {
      val model = LogisticRegressionModel.load(sc, path+"logisticmulticlass")
      val testData = calculateFeatures(rddTestData)
      val predictionsAndLabel = testData.map { point =>
        val prediction = model.predict(point.features)
        (prediction, point.label)
      }
      predictionsAndLabel
      

    } else if (algorithm.equals("naivebayes")) {
      val model = NaiveBayesModel.load(sc, path+"naivebayes")
      val testData = calculateFeatures(rddTestData)
      val predictionsAndLabel = testData.map { point =>
        val prediction = model.predict(point.features)
        (prediction, point.label)
      }
      predictionsAndLabel
      

    } else if (algorithm.equals("randomforest")) {
      
      val model = RandomForestModel.load(sc, path+"randomforest")
      val testData = calculateFeatures(rddTestData)
      val predictionsAndLabel = testData.map { point =>
        val prediction = model.predict(point.features)
        (prediction, point.label)
      }
      predictionsAndLabel

    }else{
      null
    }
     


      val multiclassMetrics = new MulticlassMetrics(predictionsAndLabel)

      println("precision: " + multiclassMetrics.precision)
      println("weightFalsePositiveRate: " + multiclassMetrics.weightedFalsePositiveRate)
      println("recall: " + multiclassMetrics.recall) //Returns recall (equals to precision for multiclass classifier because sum of all false positives is equal to sum of all false negatives) 
      println("weightTruePositiveRate:" + multiclassMetrics.weightedTruePositiveRate)
      println("fmeasure: " + multiclassMetrics.fMeasure) //Returns f-measure (equals to precision and recall because precision equals recall) 

    }

  }

  def calculateFeatures(rdd: RDD[(Int, Double, String)]): RDD[TimedLabeledPoint] = {
    val frequencyInSec = 5
    val intervalInSec = 10
    val samples = frequencyInSec * intervalInSec

    val longFft = FFT.convertPow2(samples)

    val vectorFrequency = (-frequencyInSec / 2.0 to frequencyInSec / 2.0).by(frequencyInSec.toDouble / (longFft - 1)).toArray.takeRight(longFft / 2)

    val rddDataX = rdd.map { case (timestamp, target, valuesbefore) => (timestamp, target, valuesbefore.split("@").map { x => new Complex(x.split(",").apply(0).toDouble, 0) }.toList) }
    val rddDataY = rdd.map { case (timestamp, target, valuesbefore) => (timestamp, target, valuesbefore.split("@").map { x => new Complex(x.split(",").apply(1).toDouble, 0) }.toList) }
    val rddDataZ = rdd.map { case (timestamp, target, valuesbefore) => (timestamp, target, valuesbefore.split("@").map { x => new Complex(x.split(",").apply(2).toDouble, 0) }.toList) }

    val rddTimeLabel = rdd.map { x => (x._1, (x._2 - 1)) }

    val dataFttModuleX = rddDataX.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
    val dataFttModuleY = rddDataY.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }
    val dataFttModuleZ = rddDataZ.map { case (timestamp, target, listComplex) => (timestamp, target, FFT.executeFFT(listComplex.toList).map { x => x.module() }) }.map { case (timestamp, target, list) => (timestamp, target, list.takeRight(longFft / 2)) }

    val meanAndStdX = rddDataX.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }
    val meanAndStdY = rddDataY.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }
    val meanAndStdZ = rddDataZ.map { case (timestamp, target, listComplex) => (timestamp, (calculateMean(listComplex), calculateStd(listComplex))) }

    val maxAndPosFreqX = dataFttModuleX.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }
    val maxAndPosFreqY = dataFttModuleY.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }
    val maxAndPosFreqZ = dataFttModuleZ.map {
      case (timestamp, target, listModuleComplex) =>
        val max = listModuleComplex.max
        (timestamp, (max, calculatePointMax(max, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqX = dataFttModuleX.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqY = dataFttModuleY.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    val bandwidth50And95PosFreqZ = dataFttModuleZ.map {
      case (timestamp, target, listModuleComplex) =>
        (timestamp, (caculateFreqPositionBandwidth(50, listModuleComplex, vectorFrequency), caculateFreqPositionBandwidth(95, listModuleComplex, vectorFrequency)))
    }

    // features mean, std, max, posFreq max, posFreq bw50, posFreq bw95
    val featuresX = meanAndStdX.join(maxAndPosFreqX).join(bandwidth50And95PosFreqX)
    val featuresY = meanAndStdY.join(maxAndPosFreqY).join(bandwidth50And95PosFreqY)
    val featuresZ = meanAndStdZ.join(maxAndPosFreqZ).join(bandwidth50And95PosFreqZ)

    val trainingData = rddTimeLabel.join(featuresX).join(featuresY).join(featuresZ).map {
      case (timestamp, (((label, (((meanX, stdX), (maxX, pfreqX)), (bw50X, bx95X))), (((meanY, stdY), (maxY, pfreqY)), (bw50Y, bw95Y))), (((meanZ, stdZ), (maxZ, pfreqZ)), (bw50Z, bw95Z)))) =>

        val arrayValues = Array(meanX, stdX, maxX, pfreqX, bw50X, bx95X, meanY, stdY, maxY, pfreqY, bw50Y, bw95Y, meanZ, stdZ, maxZ, pfreqZ, bw50Z, bw95Z)
        val features = Vectors.dense(arrayValues)
        new TimedLabeledPoint(timestamp, label, features)
    }
    trainingData
  }

  def calculateMean(listComplex: List[Complex]): Double = {
    listComplex.map(_.re).sum / listComplex.length
  }

  def calculateStd(listComplex: List[Complex]): Double = {
    val mean = calculateMean(listComplex)
    Math.sqrt(listComplex.map(_.re).map { x => (x - mean) * (x - mean) }.sum) / listComplex.length
  }

  def calculatePointMax(max: Double, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    vectorFrequency.apply(listModuleComplex.indexOf(max))
  }

  def caculateFreqPositionBandwidth(percentage: Int, listModuleComplex: List[Double], vectorFrequency: Array[Double]): Double = {
    val totalPercentageBandwidth = (listModuleComplex.sum * percentage) / 100.0
    var sumBandwidth = 0.0
    var position = -1
    for (i <- 0 to listModuleComplex.size - 1) {
      sumBandwidth += listModuleComplex.apply(i)

      if (position == -1) {
        if (sumBandwidth >= totalPercentageBandwidth) {
          position = i
        }
      }
    }
    vectorFrequency.apply(position)
  }
}