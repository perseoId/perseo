package com.paradigma.perseo.training.util

import org.apache.spark.SparkConf

/**
 * @author jalmodovar
 */
object SparkConfFactory extends Serializable{
  val sparkConf = new SparkConf().setAppName("trainingProcess").setMaster("local[2]").set("spark.ui.port", "7077").set("spark.mesos.coarse", "true")
      .set("spark.cassandra.connection.host", "helper3.stratio.com").set("spark.driver.allowMultipleContexts", "true")
}