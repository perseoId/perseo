var PERSEO = PERSEO || {};

(function(obj, $, RealTime, Batch, Training, Menu, Algorithm) {
   'use strict';

   obj.initContentView = 'realtime';

   obj.debug = {
      isActive: true
   };

   obj.constants = {
      algorithmType: {
         CLASSIFICATION: 'classification',
         REGRESSION: 'regression'
      },
      paths: {
         API: '/api/',
         ROOT: '/Perseo'
      },
      REFRESH_TIME: 10000,
      VIEWNAME_ALGORITHM: 'algorithm',
      VIEWNAME_BATCH: 'batch',
      VIEWNAME_REALTIME: 'realtime',
      VIEWNAME_TRAINING: 'training'
   };

   obj.status = {
      content: {},
      clean: function() {
         PERSEO.status.content = {};
      }
   };

   obj.initialize = function() {
      // Load the views when header will be loaded
      this.loadHeader(this.loadAllViews);
      this.loadFooter();
   };

   obj.changeView = function(view) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading ' + view + '...'); }

      $('[data-view="content"]').each(function() {
         if (PERSEO.debug.isActive) { console.log('[PERSEO] View ' + view + ' was loaded.'); }

         $(this).load('views/' + view + '.view.html', function() {
            PERSEO.status.clean();
            delete PERSEO.status.content.viewObject;

            switch (view) {
               case 'realtime':
                  PERSEO.status.content.viewObject = new RealTime();
                  PERSEO.status.content.viewObject.regressionChartId = 'regressionChart';
                  PERSEO.status.content.viewObject.classificationChartId = 'classificationChart';

                  break;
               case 'batch':
                  PERSEO.status.content.viewObject = new Batch();

                  break;
               case 'training':
                  PERSEO.status.content.viewObject = new Training();

                  break;
               case 'algorithm':
                  PERSEO.status.content.viewObject = new Algorithm();
                  PERSEO.status.content.viewObject.regressionChartId = 'regressionChart';
                  PERSEO.status.content.viewObject.classificationChartId = 'classificationChart';

                  break;
               default:
                  if (PERSEO.debug.isActive) { console.warn('[PERSEO] View ' + view + ' has not initialization.'); }
            }

            PERSEO.status.content.viewName = view;
            PERSEO.mainMenu.markItem(view);

            if (PERSEO.status.content.viewObject) { PERSEO.status.content.viewObject.initialize(); }
         });
      });
   };

   /** Load the header of the website */
   obj.loadHeader = function(callback) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading header...'); }

      $('[data-view="header"]').load('views/header.view.html', function() {
         if (PERSEO.debug.isActive) { console.log('[PERSEO] Header was loaded.'); }

         PERSEO.mainMenu = new Menu();
         PERSEO.mainMenu.selector = '.perseo-c-main-menu';
         PERSEO.mainMenu.itemSelector = '.perseo-c-main-menu__item';
         PERSEO.mainMenu.initialize();
         if (callback) { callback(); }
      });
   };

   /** Load the footer of the website */
   obj.loadFooter = function(callback) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading footer...'); }

      $('[data-view="footer"]').load('views/footer.view.html', function() {
         if (PERSEO.debug.isActive) { console.log('[PERSEO] Footer was loaded.'); }

         if (callback) { callback(); }
      });
   };

   /** Load the content of the current page */
   obj.loadAllViews = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading all views in the page'); }

      $('[data-view="content"]').each(function() {
         var view;

         if ($(this).attr('data-view') !== 'content') {
            view = $(this).attr('data-view');
         } else {
            view = PERSEO.initContentView;
         }

         if (view) {
            PERSEO.changeView(view);
         }
      });
   };

   obj.hideMessages = function() {
      $('.perseo-c-message').each(function() {
         $(this).hide();
      });
   };

   // Launch Perseo
   $(document).ready(function() {
      PERSEO.initialize();
   });
})(PERSEO, $, RealTime, Batch, Training, Menu, Algorithm);
