'use strict';

function Algorithm() {
   var self = this;
   var regressionChartId;
   var classificationChartId;

   this.isInitialized = false;

   this.initialize = function() {
      if (!this.isInitialized) {
         this.isInitialized = true;

         if (PERSEO.debug.isActive) { console.log('[PERSEO] Initializing Algorithm view...'); }

         PERSEO.status.content.chartTypeActive = $('[data-chart="' + self.regressionChartId + '"]').val().toLowerCase();

         if (!self.regressionChartId || !self.classificationChartId) {
            if (PERSEO.debug.isActive) {
               console.error('[PERSEO] Algorithm.regressionChartId or ' +
                             'Algorithm.classificationChartId was not defined.');
            }
         } else {
            PERSEO.status.content.chartTypeActive = $('[data-chart="' + self.regressionChartId + '"]')
                                                    .val().toLowerCase();

            this.loadTypeCharts(PERSEO.status.content.chartTypeActive);
            this.loadRegressionAlgorithms();
            this.loadClassificationAlgorithms();
            this.setEvents();
         }

         PERSEO.hideMessages();
      } else {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] Avoid Algorithm duplicate initialization...'); }
      }
   };

   this.loadTypeCharts = function(type) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Selecting chart type', type); }

      if (!type) {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] No chart type was selected. ' +
                                        PERSEO.status.content.chartTypeActive + ' will be selected.'); }

         type = PERSEO.status.content.chartTypeActive;
      }

      PERSEO.status.content.chartTypeActive = type;

      switch (type) {
         case $('[data-chart="' + self.regressionChartId + '"]').val().toLowerCase():
            $('[data-chart-reference="' + self.regressionChartId + '"]').each(function() {
               $(this).show();
            });

            $('[data-chart-reference="' + self.classificationChartId + '"]').each(function() {
               $(this).hide();
            });

            $('[data-chart=' + self.regressionChartId + ']').addClass('perseo-c-button--primary');
            $('[data-chart=' + self.classificationChartId + ']').removeClass('perseo-c-button--primary');

            break;
         default:
            $('[data-chart-reference="' + self.regressionChartId + '"]').each(function() {
               $(this).hide();
            });

            $('[data-chart-reference="' + self.classificationChartId + '"]').each(function() {
               $(this).show();
            });

            $('[data-chart=' + self.regressionChartId + ']').removeClass('perseo-c-button--primary');
            $('[data-chart=' + self.classificationChartId + ']').addClass('perseo-c-button--primary');
      };
   };

   this.setEvents = function() {
      $('input[name="chartType"]').each(function() {
         $(this).click(function() {
            self.loadTypeCharts(this.value.toLowerCase());

            if (self.timeoutId) {
               if (PERSEO.debug.isActive) { console.info('[PERSEO] Aborting timeout', self.timeoutId); }

               clearTimeout(self.timeoutId);
            }

            self.loadAlgorithms(this.value.toLowerCase());
         });
      });
   };

   this.loadRegressionAlgorithms = function() {
      self.loadAlgorithms($('[data-chart="' + self.regressionChartId + '"]')
                          .val().toLowerCase());
   };

   this.loadClassificationAlgorithms = function() {
      self.loadAlgorithms($('[data-chart="' + self.classificationChartId + '"]')
                          .val().toLowerCase());
   };

   this.loadAlgorithms = function(type) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading algorithm results.'); }
      if (!type) {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] No algorithm type was selected.'); }

         type = PERSEO.status.content.chartTypeActive;
      } else {
         if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading ' + type + ' algorithm results.'); }

         $.ajax({
            type: 'GET',
            url: PERSEO.constants.paths.ROOT + PERSEO.constants.paths.API + 'results/' + type,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,

            success: function(data) {
               if (PERSEO.debug.isActive) {
                  console.info('[PERSEO] Success on loading regression algorithm results.');
               }

               if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_ALGORITHM)) {
                  self.drawCharts(data);
                  self.timeoutId = setTimeout(self.loadAlgorithms, PERSEO.constants.REFRESH_TIME);
               }
            },

            error: function() {
               if (PERSEO.debug.isActive) {
                  console.error('[PERSEO] Error on loading regression algorithm results.');
               }

               if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_ALGORITHM)) {
                  self.timeoutId = setTimeout(self.loadAlgorithms, PERSEO.constants.REFRESH_TIME);
               }
            }
         });
      }

   };

   this.loadClassificationAlgorithms = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading classification algorithm results.'); }
   };

   this.drawCharts = function(data) {
      var datachart;
      var chart;
      var algorithms = [];
      var options = {
         animation: {
            duration: 500,
            easing: 'out',
            startup: true
         },
         chart: {
            title: '',
            subtitle: ''
         },
         colors: ['#009bdb'],
         legend: {
            position: 'none'
         },
         selectionMode: 'multiple',
         titleTextStyle: {
            color: '#999',
            fontName: 'Open Sans',
            fontSize: '18',
            bold: false,
            italic: false
         },
         tooltip: {
            trigger: 'focus'
         },
         width: 350,
         height: 250,
         margin: '20px auto',
      };

      if (PERSEO.debug.isActive) { console.log('[PERSEO] Creating charts...'); }

      for (var i = 0; i < data.length; i++) {
         datachart = new google.visualization.DataTable();

         options.title = data[i].key;
         console.log(data[i].key);
         options.chart.title = data[i].key;

         datachart.addColumn('string', 'algorithm');
         datachart.addColumn('number', 'value');

         for (var c = 0; c < data[i].resultsAlgorithm.length; c++) {
            datachart.addRow([data[i].resultsAlgorithm[c].algorithm,
                              data[i].resultsAlgorithm[c].value]);
         }

         if (PERSEO.debug.isActive) { console.log('[PERSEO] Drawing chart on #' + data[i].key); }

         chart = new google.visualization.ColumnChart(document.getElementById(data[i].key));
         //chart = new google.charts.Bar(document.getElementById(data[i].key));

         chart.draw(datachart, options);
      }
   };
}
