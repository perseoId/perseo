'use strict';

function RealTime() {
   var self = this;
   var regressionChartId;
   var classificationChartId;
   var timeoutId;

   this.isInitialized = false;

   this.initialize = function() {
      if (!this.isInitialized) {
         this.isInitialized = true;

         if (PERSEO.debug.isActive) { console.log('[PERSEO] Initializing RealTime view...'); }

         PERSEO.status.content.isRegressionChartLoaded = false;
         PERSEO.status.content.isClassificationChartLoaded = false;

         google.charts.load('current', {'packages': ['line', 'corechart', 'bar']});

         this.loadTypeChart(this.regressionChartId);
         this.setEventListeners();
      } else {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] Avoid RealTime duplicate initialization...'); }
      }
   };

   this.loadTypeChart = function(type) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading chart ' + type + '...'); }

      if (this.regressionChartId.indexOf(type) > -1) {
         PERSEO.status.content.chartActive = this.regressionChartId;
      } else if (this.classificationChartId.indexOf(type) > -1) {
         PERSEO.status.content.chartActive = this.classificationChartId;
      }

      if (self.timeoutId) {
         clearTimeout(self.timeoutId);
         self.timeoutId = undefined;
      }

      this.loadData();

      switch (PERSEO.status.content.chartActive) {
         case this.classificationChartId:
            $('#' + this.regressionChartId).hide();
            $('#' + this.classificationChartId).show();
            $('*[data-chart-reference="' + this.regressionChartId + '"]').hide();
            $('*[data-chart-reference="' + this.classificationChartId + '"]').show();

            break;
         default:
            $('#' + this.regressionChartId).show();
            $('#' + this.classificationChartId).hide();
            $('*[data-chart-reference="' + this.regressionChartId + '"]').show();
            $('*[data-chart-reference="' + this.classificationChartId + '"]').hide();
      }

      $('input[name="chartType"][data-chart=' + PERSEO.status.content.chartActive + ']')
         .addClass('perseo-c-button--primary');
      $('input[name="chartType"]:not([data-chart=' + PERSEO.status.content.chartActive + '])')
         .removeClass('perseo-c-button--primary');
   };

   this.setEventListeners = function() {
      $('input[name="chartType"]').each(function() {
         $(this).click(function() {
            self.loadTypeChart(this.value.toLowerCase());
         });
      });
   };

   this.loadDataChart = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Refresh regression chart.'); }

      if (!(PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_REALTIME) ||
           !(PERSEO.status.content.chartActive == self.regressionChartId)) {
         if (PERSEO.debug.isActive) { console.log('[PERSEO] Refresh will be aborted.'); }
      }

      $.ajax({
         type: 'GET',
         url: PERSEO.constants.paths.ROOT + PERSEO.constants.paths.API + 'predictionrealtime/chart/regression',
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         async: false,

         success: function(data) {
            if (PERSEO.debug.isActive) { console.info('[PERSEO] Success on loading regression data.'); }

            if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_REALTIME) &&
               (PERSEO.status.content.chartActive == self.regressionChartId)) {
               self.drawChart(data);
               self.timeoutId = setTimeout(self.loadDataChart, PERSEO.constants.REFRESH_TIME);
            }
         },

         error: function() {
            if (PERSEO.debug.isActive) { console.error('[PERSEO] Error on loading regression data.'); }

            if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_REALTIME) &&
               (PERSEO.status.content.chartActive == self.regressionChartId)) {
               self.timeoutId = setTimeout(self.loadDataChart, PERSEO.constants.REFRESH_TIME);
            }
         }
      });
   };

   this.drawChart = function(data) {
      var datachart = new google.visualization.DataTable();

      datachart.addColumn('string', 'Time');
      datachart.addColumn('number', 'Real value');
      datachart.addColumn('number', 'Prediction');

      for (var i = 0; i < data.length; i++) {
         var formatedDate = new Date();
         formatedDate.setSeconds(data[i].time);
         datachart.addRow([formatedDate.toTimeString().split(' ')[0], data[i].value, data[i].prediction]);
      }

      var options = {
         animation: {
            duration: 500,
            easing: 'out',
            startup: true
         },
         chart: {
            //title: 'prediction vs real value',
         },
         colors: ['#b5e2f5', '#009bdb'],
         selectionMode: 'multiple',
         tooltip: {
            trigger: 'focus'
         },
         width: 1000,
         height: 700,
         margin: '0 auto',
      };

      var chart = new google.charts.Line(document.getElementById('regressionChart'));

      chart.draw(datachart, options);
   };

   this.loadDataChartClassification = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Refresh classification chart.'); }

      $.ajax({
         type: 'GET',
         url: PERSEO.constants.paths.ROOT + PERSEO.constants.paths.API + 'predictionrealtime/chart/classification',
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         async: false,

         success: function(data) {
            if (PERSEO.debug.isActive) { console.info('[PERSEO] Success on loading classification data.'); }

            if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_REALTIME) &&
               (PERSEO.status.content.chartActive == self.classificationChartId)) {
               self.drawChartClassification(data);
               self.timeoutId = setTimeout(self.loadDataChartClassification, PERSEO.constants.REFRESH_TIME);
            }
         },

         error: function() {
            if (PERSEO.debug.isActive) { console.error('[PERSEO] Error on loading classification data.'); }

            if (data && (PERSEO.status.content.viewName == PERSEO.constants.VIEWNAME_REALTIME) &&
               (PERSEO.status.content.chartActive == self.classificationChartId)) {
               self.timeoutId = setTimeout(self.loadDataChartClassification, PERSEO.constants.REFRESH_TIME);
            }
         }
      });
   };

   this.drawChartClassification = function(data) {
      var datachart = new google.visualization.DataTable();

      datachart.addColumn('string', 'Time');
      datachart.addColumn('number', 'Real value');
      datachart.addColumn('number', 'Prediction');

      for (var i = 0; i < data.length; i++) {
         var formatedDate = new Date();
         formatedDate.setSeconds(data[i].time);
         datachart.addRow([formatedDate.toTimeString().split(' ')[0], data[i].value, data[i].prediction]);
      }

      var options = {
         animation: {
            duration: 500,
            easing: 'out',
            startup: true
         },
         chart: {
            //title: 'prediction vs real value',
            legend: {position: 'bottom'}
         },
         colors: ['#b5e2f5', '#009bdb'],
         selectionMode: 'multiple',
         height: 700,
         margin: '0 auto',
         tooltip: {
            trigger: 'focus'
         },
         vAxis: {
            gridlines: {
               count: 7
            }
         },
         width: 1000
      };

      var chart = new google.charts.Line(document.getElementById('classificationChart'));

      chart.draw(datachart, options);
   };

   this.loadData = function() {
      if (PERSEO.status.content.chartActive == this.regressionChartId) {
         if (PERSEO.status.content.isRegressionChartLoaded) {
            this.loadDataChart();
         } else {
            google.charts.setOnLoadCallback(this.loadDataChart);
            PERSEO.status.content.isRegressionChartLoaded = true;
         }
      }

      if (PERSEO.status.content.chartActive == this.classificationChartId) {
         if (PERSEO.status.content.isClassificationChartLoaded) {
            this.loadDataChartClassification();
         } else {
            google.charts.setOnLoadCallback(this.loadDataChartClassification);
            PERSEO.status.content.isClassificationChartLoaded = true;
         }
      }
   };
}
