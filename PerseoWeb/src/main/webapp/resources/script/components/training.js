'use strict';

function Training() {
   var self = this;

   this.isInitialized = false;

   this.initialize = function() {
      if (!this.isInitialized) {
         this.isInitialized = true;

         if (PERSEO.debug.isActive) { console.log('[PERSEO] Initializing Training view...'); }

         this.loadAlgorithm(PERSEO.constants.algorithmType.REGRESSION);
         this.setEvents();

         PERSEO.hideMessages();
      } else {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] Avoid Training duplicate initialization...'); }
      }
   };

   this.setEvents = function() {
      $('input[type="button"]').click(this.runTraining);

      $('#algorithmType').change(function(evt) {
         self.loadAlgorithm($(this).val());
      });
   };

   this.runTraining = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Running Training...'); }

      var algorithm = 'NONE';
      var algorithmType = PERSEO.status.content.algorithmType;

      switch (algorithmType) {
         case PERSEO.constants.algorithmType.REGRESSION:
            algorithm = $('#algorithmReg').val();

            break;
         case PERSEO.constants.algorithmType.CLASSIFICATION:
            algorithm = $('#algorithmClass').val();

            break;
         default:
            if (PERSEO.debug.isActive) { console.warn('[PERSEO] No algorithm was selected for training.'); }
      }

      var jsonObj = {
         'typeAlgorithm': algorithmType,
         'algorithm': algorithm
      };

      if (PERSEO.debug.isActive) { console.log('[PERSEO] JSON to /api/training/execute', jsonObj); }

      $.ajax({
         type: 'POST',
         url: PERSEO.constants.paths.ROOT + PERSEO.constants.paths.API + 'training/execute',
         contentType: 'application/json; charset=utf-8',
         dataType: 'application/json',
         data: JSON.stringify(jsonObj)
      }).done(function(data) {
            if (PERSEO.debug.isActive) { console.log('[PERSEO] /api/training/execute response', data); }

            self.success(data.message);
         }).fail(function(data) {
            if (PERSEO.debug.isActive) { console.error('[PERSEO] /api/training/execute response failed.', data); }

            if (data.statusText.toLowerCase() === 'ok') {
               if (JSON.parse(data.responseText).result.toLowerCase() === 'ok') {
                  self.success(JSON.parse(data.responseText).message);
               } else {
                  self.err(JSON.parse(data.responseText).message);                     
               }
            } else {
               self.err('The call to the remote service failed or it is impossible read your file. Check your file to ensure is readable and have a valid format or contact with your administrator. ');
            }
         }
    );
   };

   this.loadAlgorithm = function(algorithmType) {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Loading algorithm ' + algorithmType + '...'); }

      switch (algorithmType) {
         case PERSEO.constants.algorithmType.CLASSIFICATION:
            $('#algorithmTypeRegression').hide();
            $('#algorithmTypeClassification').show();

            break;
         default:
            $('#algorithmTypeRegression').show();
            $('#algorithmTypeClassification').hide();
      }

      PERSEO.status.content.algorithmType = algorithmType;
   };

   this.err = function(message) {
      $('.perseo-c-message').removeClass('perseo-c-message--error perseo-c-message--success').addClass('perseo-c-message--error');
      $('.perseo-c-message').text(message);
      $('.perseo-c-message').show();
   };

   this.success = function(message) {
      $('.perseo-c-message').removeClass('perseo-c-message--error perseo-c-message--success').addClass('perseo-c-message--success');
      $('.perseo-c-message').text(message);
      $('.perseo-c-message').show();
   };}
