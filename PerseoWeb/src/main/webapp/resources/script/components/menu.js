'use strict';

function Menu() {
   var self = this;

   this.selector;
   this.itemSelector;
   this.items = [];
   this.activeItem;
   this.ACTIVE_CLASS = 'perseo-c-main-menu__item--active';
   this.isInitialized = false;

   this.initialize = function() {
      if (!this.isInitialized) {
         this.isInitialized = true;
         this.loadItems();
         this.seekActiveItem();
         this.markActiveItem();
         this.addItemEvents();
      } else {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] Avoid Menu duplicate initialization...'); }
      }
   };

   this.loadItems = function() {
      var menu = this;

      $(this.itemSelector).each(function(index) {
         menu.items.push($(this));
      });
   };

   this.addItemEvents = function() {
      if (this.items) {
         for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].attr('data-view') != this.activeItem.attr('data-view')) {
               this.items[i].attr('disabled', false);

               if (this.items[i].attr('data-bind') != 'true') {
                  this.items[i].attr('data-bind', 'true');

                  this.items[i].click(function(evt) {
                     self.unmarkActiveItem();
                     self.activeItem = $(this);
                     self.addItemEvents();
                     PERSEO.changeView(evt.currentTarget.attributes['data-view'].value);
                  });
               }
            } else {
               this.activeItem.attr('disabled', true);
               this.activeItem.unbind('click');
               this.activeItem.attr('data-bind', 'false');
            }
         }
      }
   };

   this.seekActiveItem = function() {
      var contentView;

      if (PERSEO.status.content.viewName) {
         contentView = PERSEO.status.content.viewName;
      } else {
         contentView = PERSEO.initContentView;
      }

      if (this.items) {
         for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].attr('data-view') == contentView) {
               this.activeItem = this.items[i];

               break;
            }
         }
      }
   };

   this.markActiveItem = function() {
      if (this.activeItem) { this.activeItem.addClass(this.ACTIVE_CLASS); }
   };

   this.markItem = function(viewName) {
      if (this.items) {
         for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].attr('data-view') == viewName) {
               this.unmarkActiveItem();
               this.activeItem = this.items[i];
               this.markActiveItem();

               break;
            }
         }
      }
   };

   this.unmarkActiveItem = function() {
      if (this.activeItem) {
         this.activeItem.removeClass(this.ACTIVE_CLASS);
      }
   };
};
