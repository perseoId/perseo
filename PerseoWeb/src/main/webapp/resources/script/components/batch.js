'use strict';

function Batch() {
   var self = this;

   this.isInitialized = false;

   this.initialize = function() {
      if (!this.isInitialized) {
         this.isInitialized = true;

         if (PERSEO.debug.isActive) { console.log('[PERSEO] Initializing Batch view...'); }

         this.setEvents();

         PERSEO.hideMessages();
      } else {
         if (PERSEO.debug.isActive) { console.warn('[PERSEO] Avoid Batch duplicate initialization...'); }
      }
   };

   this.setEvents = function() {
      $('input[type="button"]').click(this.runBatch);
   };

   this.runBatch = function() {
      if (PERSEO.debug.isActive) { console.log('[PERSEO] Running Batch...'); }

      var fileName;

      if (document.getElementById('file').files.length > 0) {
         fileName = document.getElementById('file').files[0].name;

         var numberOfSamples = parseInt($('#numberOfSamples').val());
         var algorithmType = $('#algorithmType').val();
         var input = document.getElementById('file').files[0];
         var reader = new FileReader();
   
         reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            var jsonObj = {
               'typeAlgorithm': algorithmType,
               'numberOfSamples': numberOfSamples,
               'fileName': fileName,
               'fileBase64': btoa(binaryString)
            };

            if (PERSEO.debug.isActive) { console.log('[PERSEO] JSON to /api/batch/execute', jsonObj); }

            $.ajax({
               type: 'POST',
               url: PERSEO.constants.paths.ROOT + PERSEO.constants.paths.API + 'batch/execute',
               contentType: 'application/json; charset=utf-8',
               dataType: 'application/json',
               data: JSON.stringify(jsonObj),

               success: function(data) {
                  if (PERSEO.debug.isActive) { console.log('[PERSEO] /api/batch/execute response', data); }

                  if (data.result === 'ok') {
                     self.success(data.message);
                  } else {
                     self.err(data.message);
                  }
               },

               error: function(data) {
                  if (PERSEO.debug.isActive) { console.error('[PERSEO] /api/batch/execute response failed or is not possible read your file.', data); }

                  if (data.statusText.toLowerCase() === 'ok') {
                     if (JSON.parse(data.responseText).result.toLowerCase() === 'ok') {
                        self.success(JSON.parse(data.responseText).message);
                     } else {
                        self.err(JSON.parse(data.responseText).message);                     
                     }
                  } else {
                     self.err('The call to the remote service failed or it is impossible read your file. Check your file to ensure is readable and have a valid format or contact with your administrator. ');
                  }
               }
            });
         };

         reader.readAsBinaryString(input);
      } else {
         self.err('There is no file selected. Please, select one to continue.');
      }
   };

   this.err = function(message) {
      $('.perseo-c-message').removeClass('perseo-c-message--error perseo-c-message--success').addClass('perseo-c-message--error');
      $('.perseo-c-message').text(message);
      $('.perseo-c-message').show();
   };

   this.success = function(message) {
      $('.perseo-c-message').removeClass('perseo-c-message--error perseo-c-message--success').addClass('perseo-c-message--success');
      $('.perseo-c-message').text(message);
      $('.perseo-c-message').show();
   };
}
