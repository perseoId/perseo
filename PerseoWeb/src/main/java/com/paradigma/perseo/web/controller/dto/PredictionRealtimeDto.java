package com.paradigma.perseo.web.controller.dto;

public class PredictionRealtimeDto {

	private int time;
	private double value;
	private double prediction;
	
	public PredictionRealtimeDto() {
		super();
	}

	public PredictionRealtimeDto(int time, double value, double prediction) {
		super();
		this.time = time;
		this.value = value;
		this.prediction = prediction;
	}

	public int getTime() {
		return time;
	}


	public void setTime(int time) {
		this.time = time;
	}


	public double getValue() {
		return value;
	}


	public void setValue(double value) {
		this.value = value;
	}


	public double getPrediction() {
		return prediction;
	}


	public void setPrediction(double prediction) {
		this.prediction = prediction;
	}


	@Override
	public String toString() {
		return "PredictionRealtimeDto [time=" + time + ", value=" + value
				+ ", prediction=" + prediction + "]";
	}
	
	
}
