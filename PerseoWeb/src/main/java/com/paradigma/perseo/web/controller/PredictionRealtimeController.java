package com.paradigma.perseo.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.perseo.web.config.CassandraConfig;
import com.paradigma.perseo.web.controller.dto.PredictionRealtimeDto;
import com.paradigma.perseo.web.domain.PredictionRealtime;
import com.paradigma.perseo.web.service.CassandraService;
import com.paradigma.perseo.web.service.PredictionsRealtimeMapper;


@Controller
@RequestMapping("/api/predictionrealtime")
@Import(CassandraConfig.class)
public class PredictionRealtimeController {

	@Resource
	private CassandraService cassandraServiceRealTime;

	@RequestMapping(value = "/chart/regression", method = RequestMethod.GET)
	public @ResponseBody List<PredictionRealtimeDto> getPredictionRealtimes() {
		List <PredictionRealtime> prtList=cassandraServiceRealTime.getPredictionRealtime();
		List <PredictionRealtimeDto> prtDtoList = new ArrayList<>();
		PredictionsRealtimeMapper prtMapper= new PredictionsRealtimeMapper();
		for(PredictionRealtime prt: prtList){
			prtDtoList.add(prtMapper.parserCassandraToDto(prt));
		}
		return sortList(prtDtoList);
				
		
	}
	
	@RequestMapping(value = "/chart/classification", method = RequestMethod.GET)
	public @ResponseBody List<PredictionRealtimeDto> getPredictionRealtimesClassification() {
		List <PredictionRealtime> prtList=cassandraServiceRealTime.getPredictionRealtimeClassification();
		List <PredictionRealtimeDto> prtDtoList = new ArrayList<>();
		PredictionsRealtimeMapper prtMapper= new PredictionsRealtimeMapper();
		for(PredictionRealtime prt: prtList){
			prtDtoList.add(prtMapper.parserCassandraToDto(prt));
		}
		return sortList(prtDtoList);
				
		
	}
	
	private List<PredictionRealtimeDto> sortList(List<PredictionRealtimeDto> prtDtoList){
		Collections.sort(prtDtoList, new Comparator<PredictionRealtimeDto>() {
            @Override
            public int compare(PredictionRealtimeDto lhs, PredictionRealtimeDto rhs) {
                return lhs.getTime() < rhs.getTime() ? -1 : (lhs.getTime() > rhs.getTime() ) ? 1 : 0;
            }
        });
		return prtDtoList;
	}

}
