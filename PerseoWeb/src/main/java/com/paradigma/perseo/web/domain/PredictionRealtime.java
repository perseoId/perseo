package com.paradigma.perseo.web.domain;

public class PredictionRealtime {

	private int keyTime;
	private double keyValue;
	private double predictionValue;
	
	
	public PredictionRealtime() {
		super();
	}
	
	public PredictionRealtime(int keyTime, double keyValue,
			double predictionValue) {
		super();
		this.keyTime = keyTime;
		this.keyValue = keyValue;
		this.predictionValue = predictionValue;
	}

	public int getKeyTime() {
		return keyTime;
	}

	public void setKeyTime(int keyTime) {
		this.keyTime = keyTime;
	}

	public double getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(double keyValue) {
		this.keyValue = keyValue;
	}

	public double getPredictionValue() {
		return predictionValue;
	}

	public void setPredictionValue(double predictionValue) {
		this.predictionValue = predictionValue;
	}

	@Override
	public String toString() {
		return "PredictionRealtime [keyTime=" + keyTime + ", keyValue="
				+ keyValue + ", predictionValue=" + predictionValue + "]";
	}
	
	
	
}
