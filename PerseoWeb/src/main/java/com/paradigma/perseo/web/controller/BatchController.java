package com.paradigma.perseo.web.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.perseo.batch.process.BatchProcess;
import com.paradigma.perseo.web.controller.dto.BatchRequestDto;
import com.paradigma.perseo.web.controller.dto.ResultDto;

//import com.paradigma.perseo.batch.process.BatchProcess;

@Controller
@RequestMapping("/api/batch")
public class BatchController {

	@RequestMapping(value = "/execute", method = RequestMethod.POST)
	public @ResponseBody ResultDto executeBatch(
			@RequestBody BatchRequestDto batchRequestDto) {
		ResultDto rdto = new ResultDto();

		if (batchRequestDto.getNumberOfSamples() < 0) {
			rdto.setResult("KO");
			rdto.setMessage("Error!! Number of samples is <0");
		} else {
			try {
				byte[] data = DatatypeConverter
						.parseBase64Binary(batchRequestDto.getFileBase64());
				String filePath = "/tmp/batchconvert.csv";
				File file = new File(filePath);
				file.delete();
				try (OutputStream stream = new FileOutputStream(filePath)) {
					stream.write(data);
				}
				System.out.println("*******BATCH********"
						+ batchRequestDto.getFileName() + ", "
						+ batchRequestDto.getNumberOfSamples());
				/*BatchProcess.process(filePath,
						batchRequestDto.getNumberOfSamples(),
						batchRequestDto.getTypeAlgorithm());*/
				rdto.setResult("OK");
				rdto.setMessage("Process Transform is completed!!");
			} catch (Exception ex) {
				rdto.setResult("KO");
				rdto.setMessage("Error in process transform.");
			}
		}

		return rdto;
	}

}
