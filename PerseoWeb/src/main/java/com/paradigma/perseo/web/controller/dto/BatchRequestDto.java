package com.paradigma.perseo.web.controller.dto;

public class BatchRequestDto {

	private int numberOfSamples;
	private String typeAlgorithm;
	private String fileName;
	private String fileBase64;

	public BatchRequestDto() {
		super();
	}

	public int getNumberOfSamples() {
		return numberOfSamples;
	}

	public void setNumberOfSamples(int numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public String getTypeAlgorithm() {
		return typeAlgorithm;
	}

	public void setTypeAlgorithm(String typeAlgorithm) {
		this.typeAlgorithm = typeAlgorithm;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileBase64() {
		return fileBase64;
	}

	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}

}
