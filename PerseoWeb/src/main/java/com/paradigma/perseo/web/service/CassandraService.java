package com.paradigma.perseo.web.service;

import java.util.List;

import org.springframework.cassandra.core.CqlOperations;

import com.paradigma.perseo.web.domain.PredictionRealtime;
import com.paradigma.perseo.web.domain.ResultsAlgorithm;


public class CassandraService {

	private CqlOperations template;
	private PredictionsRealtimeMapper predictionsRealtimeMapper;
	private ResultsAlgorithmMapper resultsAlgorithmMapper;

	private String SELECT_CQL_PREDICTIONREALTIME = "select * from perseo.predictionsrealtime ";
	private String SELECT_CQL_PREDICTIONREALTIME_CLASSIFICATION = "select * from perseo.predictionsrealtimeclass ";
	
	private String SELECT_CQL_CLASSIFICATION_RESULTS = "select * from perseo.classificationresults ";
	private String SELECT_CQL_REGRESSION_RESULTS = "select * from perseo.regressionresults ";

	public CassandraService(CqlOperations template, PredictionsRealtimeMapper predictionsRealtimeMapper) {
		this.template = template;
		this.predictionsRealtimeMapper= predictionsRealtimeMapper;
	}
	
	public CassandraService(CqlOperations template, ResultsAlgorithmMapper resultsAlgorithmMapper) {
		this.template = template;
		this.resultsAlgorithmMapper= resultsAlgorithmMapper;
	}
	
	public List<PredictionRealtime> getPredictionRealtime(){
		List<PredictionRealtime> prtList= template.query(SELECT_CQL_PREDICTIONREALTIME, predictionsRealtimeMapper);
		return prtList;
	}	
	
	public List<PredictionRealtime> getPredictionRealtimeClassification(){
		List<PredictionRealtime> prtList= template.query(SELECT_CQL_PREDICTIONREALTIME_CLASSIFICATION, predictionsRealtimeMapper);
		return prtList;
	}
	
	public List<ResultsAlgorithm> getResultsClassification(){
		List<ResultsAlgorithm> listResults = template.query(SELECT_CQL_CLASSIFICATION_RESULTS, resultsAlgorithmMapper);
		return listResults;
	}
	
	public List<ResultsAlgorithm> getResultsRegression(){
		List<ResultsAlgorithm> listResults = template.query(SELECT_CQL_REGRESSION_RESULTS, resultsAlgorithmMapper);
		return listResults;
	}

}
