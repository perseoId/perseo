package com.paradigma.perseo.web.domain;

public class ResultsAlgorithm {

	private String key;
	private String algorithm;
	private double value;
	
	public ResultsAlgorithm() {
		super();
	}

	public ResultsAlgorithm(String key, String algorithm, double value) {
		super();
		this.key = key;
		this.algorithm = algorithm;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
}
