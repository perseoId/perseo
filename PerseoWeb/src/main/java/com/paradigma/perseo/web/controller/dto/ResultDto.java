package com.paradigma.perseo.web.controller.dto;

public class ResultDto {
	
	private String result;
	private String message;

	public ResultDto(){
		super();
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
