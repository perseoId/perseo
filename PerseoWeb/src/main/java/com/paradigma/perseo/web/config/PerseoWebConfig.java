package com.paradigma.perseo.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.paradigma.perseo.web.controller")
public class PerseoWebConfig {
}
