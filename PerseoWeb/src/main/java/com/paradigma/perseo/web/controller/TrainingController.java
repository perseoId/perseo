package com.paradigma.perseo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.perseo.training.process.TrainingClassificationProcess;
import com.paradigma.perseo.training.process.TrainingRegressionProcess;
//import com.paradigma.perseo.training.process.TrainingClassificationProcess;
//import com.paradigma.perseo.training.process.TrainingRegressionProcess;
import com.paradigma.perseo.web.controller.dto.ResultDto;
import com.paradigma.perseo.web.controller.dto.TrainingRequestDto;

@Controller
@RequestMapping("/api/training")
public class TrainingController {
	
	@RequestMapping(value = "/execute", method = RequestMethod.POST)
	public @ResponseBody ResultDto executeBatch(
			@RequestBody TrainingRequestDto trainingRequestDto) {
		
		System.out.println("********TRAINING********"+trainingRequestDto.getAlgorithm());
		ResultDto rdto=new ResultDto();
		try{
			
	
		if(trainingRequestDto.getTypeAlgorithm().equals("regression")){
			rdto.setResult("OK");
			rdto.setMessage("Process training is completed!!");
			//TrainingRegressionProcess.training(trainingRequestDto.getAlgorithm().toLowerCase());
			
		}
		else if(trainingRequestDto.getTypeAlgorithm().equals("classification")){
			rdto.setResult("OK");
			rdto.setMessage("Process training is completed!!");
			//TrainingClassificationProcess.training(trainingRequestDto.getAlgorithm().toLowerCase());
			
			
		}
		else{
			rdto.setResult("KO");
			rdto.setMessage("Error in process training");
		}
		}catch(Exception e){
			rdto.setResult("KO");
			rdto.setMessage("Error in process training");
		}	
		
		return rdto;
	}

}
