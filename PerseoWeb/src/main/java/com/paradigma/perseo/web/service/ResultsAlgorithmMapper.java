package com.paradigma.perseo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cassandra.core.RowMapper;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.paradigma.perseo.web.controller.dto.ResultsAlgorithmsDto;
import com.paradigma.perseo.web.domain.ResultsAlgorithm;

public class ResultsAlgorithmMapper implements RowMapper<ResultsAlgorithm> {

	@Override
	public ResultsAlgorithm mapRow(Row row, int rowNum) throws DriverException {
		ResultsAlgorithm results = new ResultsAlgorithm();
		results.setAlgorithm(row.getString("algorithm"));
		results.setKey(row.getString("keystring"));
		results.setValue(row.getDouble("valuekey"));
		return results;
	}

	public List<ResultsAlgorithmsDto> toDto(List<ResultsAlgorithm> list) {
		List<ResultsAlgorithmsDto> dtoList = new ArrayList<>();
		for (String key : getKeys(list)) {
			ResultsAlgorithmsDto addResult = new ResultsAlgorithmsDto();
			for (ResultsAlgorithm result : list) {
				if (key.equals(result.getKey())) {
					ResultsAlgorithmsDto aux = containsKey(dtoList, key);
					if (aux.getKey() != null) {
						aux.addResultsAlgorithm(result);
						addResult.setResultsAlgorithm(aux.getResultsAlgorithm());
						
					} else {
						addResult.setKey(key);
						addResult.addResultsAlgorithm(result);
					}

				}
			}
			dtoList.add(addResult);
		}
		return dtoList;
	}

	private List<String> getKeys(List<ResultsAlgorithm> list) {
		List<String> keys = new ArrayList<>();
		for (ResultsAlgorithm result : list) {
			if (!keys.contains(result.getKey())) {
				keys.add(result.getKey());
			}
		}
		return keys;
	}

	private ResultsAlgorithmsDto containsKey(
			List<ResultsAlgorithmsDto> dtoList, String key) {
		ResultsAlgorithmsDto resultDto = new ResultsAlgorithmsDto();
		for (ResultsAlgorithmsDto auxDto : dtoList) {
			if (auxDto.getKey().equals(key)) {
				resultDto.setKey(key);
				resultDto.setResultsAlgorithm(auxDto.getResultsAlgorithm());
			}
		}
		return resultDto;
	}

}
