package com.paradigma.perseo.web.controller.dto;

public class TrainingRequestDto {

	private String typeAlgorithm;
	private String algorithm;

	public TrainingRequestDto() {
		super();
	}

	public String getTypeAlgorithm() {
		return typeAlgorithm;
	}

	public void setTypeAlgorithm(String typeAlgorithm) {
		this.typeAlgorithm = typeAlgorithm;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

}
