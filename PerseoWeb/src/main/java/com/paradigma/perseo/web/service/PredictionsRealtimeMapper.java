package com.paradigma.perseo.web.service;

import org.springframework.cassandra.core.RowMapper;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.paradigma.perseo.web.controller.dto.PredictionRealtimeDto;
import com.paradigma.perseo.web.domain.PredictionRealtime;


public class PredictionsRealtimeMapper implements RowMapper<PredictionRealtime>{

	@Override
	public PredictionRealtime mapRow(Row row, int rowNum)
			throws DriverException {
		PredictionRealtime prt = new PredictionRealtime();
		prt.setKeyTime(row.getInt("keytime"));
		prt.setKeyValue(row.getDouble("valuekey"));
		prt.setPredictionValue(row.getDouble("valuepredict"));
		return prt;
	}
	
	public PredictionRealtime parserDtotoCassandraRow(PredictionRealtimeDto predictionRealtimeDto){
		PredictionRealtime prt = new PredictionRealtime();
		prt.setKeyTime(predictionRealtimeDto.getTime());
		prt.setKeyValue(predictionRealtimeDto.getValue());
		prt.setPredictionValue(predictionRealtimeDto.getPrediction());
		return prt;
	}
	
	public PredictionRealtimeDto parserCassandraToDto(PredictionRealtime predictionRealtime){
		PredictionRealtimeDto prtDto = new PredictionRealtimeDto();
		prtDto.setTime(predictionRealtime.getKeyTime());
		prtDto.setValue(predictionRealtime.getKeyValue());
		prtDto.setPrediction(predictionRealtime.getPredictionValue());
		return prtDto;
	}
}
