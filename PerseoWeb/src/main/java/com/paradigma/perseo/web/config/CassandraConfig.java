package com.paradigma.perseo.web.config;

import org.springframework.cassandra.config.java.AbstractSessionConfiguration;
import org.springframework.cassandra.core.CqlTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.convert.CassandraConverter;
import org.springframework.data.cassandra.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;

import com.paradigma.perseo.web.service.CassandraService;
import com.paradigma.perseo.web.service.PredictionsRealtimeMapper;
import com.paradigma.perseo.web.service.ResultsAlgorithmMapper;



@Configuration
public class CassandraConfig extends AbstractSessionConfiguration{


	    @Override
	    public String getKeyspaceName() {
	        return "perseo";
	    }
	    
	    @Bean
	    public CassandraClusterFactoryBean cluster() {

	      CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
	      cluster.setContactPoints("10.200.0.115");
	      cluster.setPort(9042);

	      return cluster;
	    }
	    
	    @Bean
	    public CassandraMappingContext mappingContext() {
	      return new BasicCassandraMappingContext();
	    }

	    @Bean
	    public CassandraConverter converter() {
	      return new MappingCassandraConverter(mappingContext());
	    }
	    
	    @Bean
	    public CassandraSessionFactoryBean session() throws Exception {

	      CassandraSessionFactoryBean session = new CassandraSessionFactoryBean();
	      session.setCluster(cluster().getObject());
	      session.setConverter(converter());
	      session.setSchemaAction(SchemaAction.NONE);

	      return session;
	    }
	    
	    @Bean
	    public CassandraService cassandraServiceRealTime() throws Exception{

	    	return new CassandraService(new CqlTemplate(session().getObject()), new PredictionsRealtimeMapper());
	    }
	    
	    @Bean
	    public CassandraService cassandraServiceResultsAlgorithm() throws Exception{

	    	return new CassandraService(new CqlTemplate(session().getObject()), new ResultsAlgorithmMapper());
	    }
  
}
