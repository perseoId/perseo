package com.paradigma.perseo.web.controller;




import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.perseo.web.controller.dto.ResultsAlgorithmsDto;
import com.paradigma.perseo.web.service.CassandraService;
import com.paradigma.perseo.web.service.ResultsAlgorithmMapper;


@Controller
@RequestMapping("/api/results")
public class ResultsAlgorithmController {
	
	@Resource
	private CassandraService cassandraServiceResultsAlgorithm;
	
	@RequestMapping(value = "/classification", method = RequestMethod.GET)
	public @ResponseBody List<ResultsAlgorithmsDto>  resultsClassification() {
		ResultsAlgorithmMapper mapper = new ResultsAlgorithmMapper();
		return mapper.toDto(cassandraServiceResultsAlgorithm.getResultsClassification());
	}
	
	@RequestMapping(value = "/regression", method = RequestMethod.GET)
	public @ResponseBody List<ResultsAlgorithmsDto>  resultsRegression() {
		ResultsAlgorithmMapper mapper = new ResultsAlgorithmMapper();
		return mapper.toDto(cassandraServiceResultsAlgorithm.getResultsRegression());
	}

}
