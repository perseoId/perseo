package com.paradigma.perseo.web.controller.dto;

import java.util.ArrayList;
import java.util.List;

import com.paradigma.perseo.web.domain.ResultsAlgorithm;

public class ResultsAlgorithmsDto {

	private String key;
	private List<ResultsAlgorithm> resultsAlgorithm;
	
	public ResultsAlgorithmsDto() {
		super();
		resultsAlgorithm=new ArrayList<>();
	}
	
	public ResultsAlgorithmsDto(String key) {
		super();
		this.key=key;
		resultsAlgorithm=new ArrayList<>();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<ResultsAlgorithm> getResultsAlgorithm() {
		return resultsAlgorithm;
	}

	public void setResultsAlgorithm(List<ResultsAlgorithm> resultsAlgorithm) {
		this.resultsAlgorithm = resultsAlgorithm;
	}
	
	public boolean addResultsAlgorithm(ResultsAlgorithm resultAlgorithm){
		return resultsAlgorithm.add(resultAlgorithm);
	}
	
	
	
}
