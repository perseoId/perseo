package com.paradigma.perseo.batch.process

object BatchMain {
  def main(args: Array[String]) {
    if (args.length != 3) {
      System.err.println(
        s"""
           |Usage: BatchMode <filepath> <numberOfSamples> <type> 
           |  <filepath> is the path of the file
           |  <numberOfSamples> is the number of samples to save
           |  <type> is the form to predict. classification | regression
           |
        """.stripMargin)
      System.exit(1)
    } else {
      if (!args.apply(2).toString().toLowerCase().equals("regression") && !args.apply(2).toString().toLowerCase().equals("classification")) {
        System.err.println("<type> is not valid: " + args.apply(2).toString().toLowerCase() + ". Must choose classification|regression")
        System.exit(1)
      } else {
        val dataArray = BatchProcess.process(args.apply(0).toLowerCase(), args.apply(1).toInt, args.apply(2).toLowerCase())
        //dataArray.foreach(println)
      }
    }
  }
}