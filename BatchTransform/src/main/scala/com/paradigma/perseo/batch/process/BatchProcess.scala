package com.paradigma.perseo.batch.process

import scala.io.Source
import org.apache.spark.SparkContext
import com.paradigma.perseo.batch.util.SparkConfFactory
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.sql.SaveMode

object BatchProcess {

  def process(pathfilename: String, numberOfSamples: Integer, typeAlgorithm: String): Array[((Long, Double), String)] = {
    val start = System.currentTimeMillis()
    val sc = new SparkContext(SparkConfFactory.sparkConf)

    val bufferedSource = Source.fromFile(pathfilename)

    var rows = scala.collection.mutable.ArrayBuffer.empty[Array[String]]

    for (line <- bufferedSource.getLines.drop(1)) {
      rows += line.split(',').map(_.trim)
    }

    bufferedSource.close

    val dataArray: Array[((Long, Double), String)] = if (typeAlgorithm.equals("regression")) {
      rows.map(row => (row.head.toLong, row.last.toDouble)).reverse.sliding(numberOfSamples + 1).map({
        case list => ((list.head._1, list.head._2), list.drop(1).toList.map({ case (timestamp, value) => value }))
      }).filter { case (key, listValue) => listValue.size == numberOfSamples }.map { case (key, listValue) => (key, listValue.mkString("@")) }.toArray.sortBy(_._1)
    } else {
      //classification
      rows.map(row => (row.head.toLong, row.apply(1).toDouble, row.apply(2).toDouble, row.apply(3).toDouble, row.last.toDouble)).reverse.sliding(numberOfSamples + 1).map({
        case list => ((list.head._1), list.drop(1).toList.map({ case (timestamp, x, y, z, target) => x + "," + y + "," + z }), list.drop(1).toList.map({ case (timestamp, x, y, z, target) => target }))
      }).filter { case (key, listValue, listTargets) => listValue.size == numberOfSamples }.map { case (key, listValue, listTargets) => ((key, getMode(listTargets)), listValue.mkString("@")) }.toArray.sortBy(_._1)
    }
    val end = System.currentTimeMillis()

    println("Elapsed time: '" + (end - start) / 1000.0 + "' seconds")
    //modeSave Cassandra
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("CREATE KEYSPACE IF NOT EXISTS perseo WITH replication = {'class': 'SimpleStrategy','replication_factor': '1'};")
      session.execute("USE perseo;")
      session.execute("CREATE TABLE IF NOT EXISTS datatrainingregression(keytime int PRIMARY KEY, valuekey Double, valuepredict Double, valuesbefore text);")
      session.execute("CREATE TABLE IF NOT EXISTS datatrainingclassification(keytime int PRIMARY KEY, valuekey Double, valuepredict Double, valuesbefore text);")

      for (data <- dataArray) {
        if (typeAlgorithm.equals("regression")) {
          session.execute("INSERT INTO datatrainingregression (keytime, valuekey, valuesbefore) values(" + data._1._1 + "," + data._1._2 + ",'" + data._2 + "')")
        } else {
          //classification
          session.execute("INSERT INTO datatrainingclassification (keytime, valuekey, valuesbefore) values(" + data._1._1 + "," + data._1._2 + ",'" + data._2 + "')")
        }
      }
      session.close();

    }

    dataArray

  }

  private def getMode(input: List[Double]): Double = {
    input.groupBy(x => x).mapValues(_.length).toArray.sortBy(_._2).reverse(0)._1
  }

}