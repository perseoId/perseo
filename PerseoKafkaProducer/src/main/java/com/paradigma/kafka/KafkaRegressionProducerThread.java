package com.paradigma.kafka;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaRegressionProducerThread extends Thread {

	public KafkaRegressionProducerThread() {
		super();
	}

	
	
	
	//run regression
	@Override
	public void run() {

		//send one each 1 second
		Properties props = new Properties();

		props.put("bootstrap.servers", "helper3.stratio.com:9092");
		//props.put("bootstrap.servers", "localhost:9092");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("request.required.acks", "1");
		props.put("producer.type", "async");
		props.put("key.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");

		int i = 0;
		try {
			
			FileInputStream fstream = new FileInputStream(
					"/tmp/housepowertest.csv");
			DataInputStream entrada = new DataInputStream(fstream);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(
					entrada));
			String strLinea;
			while ((strLinea = buffer.readLine()) != null) {

				i = i + 1;
				KafkaProducer<String, String> producer = new KafkaProducer<String, String>(
						props);
				producer.send(new ProducerRecord<String, String>("topicRegressionPerseo",
						"kafkaPrueba", strLinea));
				producer.close();
				try {
					sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e);
				}
			}

			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
