package com.paradigma.kafka;

public class KafkaProducer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Start kafka producer");

		if (args.length != 1) {
			System.err.println("args: classification|regression");
			System.exit(1);
		} else {
			String type = args[0].toLowerCase();
			if (type.equals("regression")) {
				KafkaRegressionProducerThread thread=new KafkaRegressionProducerThread();
				thread.start();

			} else if (type.equals("classification")) {
				KafkaClassificationProducerThread thread = new KafkaClassificationProducerThread();
				thread.start();

			} else {
				System.err.println("args: classification|regression");
				System.exit(1);
			}
		}

		
	}

}
