package com.paradigma.kafka;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaClassificationProducerThread extends Thread {

	public KafkaClassificationProducerThread() {
		super();
	}

	
	//run classification
	@Override
	public void run() {

		Properties props = new Properties();

		props.put("bootstrap.servers", "helper3.stratio.com:9092");
		//props.put("bootstrap.servers", "172.18.65.21:32768");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("request.required.acks", "1");
		props.put("producer.type", "async");
		props.put("key.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer",
				"org.apache.kafka.common.serialization.StringSerializer");

		int i = 0;
		try {

			FileInputStream fstream = new FileInputStream(
					"/tmp/classification.csv");
			DataInputStream entrada = new DataInputStream(fstream);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(
					entrada));
			String strLinea;
			while ((strLinea = buffer.readLine()) != null) {

				i = i + 1;
				KafkaProducer<String, String> producer = new KafkaProducer<String, String>(
						props);
				producer.send(new ProducerRecord<String, String>("topicClassificationPerseo",
						"kafkaPrueba", strLinea));
				producer.close();

			}

			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
